'use strict';

var gulp = require('gulp');
var concat = require('gulp-concat');
var rename = require('gulp-rename');
/*var uglify = require('gulp-uglify');*/

var liferayThemeTasks = require('liferay-theme-tasks');

var jsFiles = [
	'src/js/vendor/bootstrap-datepicker.min.js',	
];
var jsDest = 'build/js/vendor';

liferayThemeTasks.registerTasks({
	gulp: gulp,
	hookFn: function (gulp) {
		gulp.hook('before:build:src', function (done) {
			// Fires before build:src task
			return gulp.src(jsFiles)
				.pipe(concat('scripts.js'))
				.pipe(gulp.dest(jsDest))
				.pipe(rename('scripts.min.js'))
				//.pipe(uglify())
				.pipe(gulp.dest(jsDest))
				.on('end', done);
		});
	}
});