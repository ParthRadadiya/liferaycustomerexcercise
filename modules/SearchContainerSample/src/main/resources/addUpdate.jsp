<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
<%@ include file="/init.jsp" %>

<portlet:actionURL name="addUpdateStudentInfo" var="addUpdateEmploeeInfo" />
<portlet:renderURL windowState="normal" var="backURL">
	<portlet:param name="jspPage" value="/view.jsp"></portlet:param>
</portlet:renderURL>

<liferay-ui:header backURL="${backURL}" title="Back" />
<aui:form name="studentInfo" action="${addUpdateStudentInfo}" method="post">
	
	<aui:input name="studentId" type="hidden" label="StudentId" 	value="${studentInfo.studentId}" />
	
	<aui:input name="studentName" label="StudentName" value="${studentInfo.studentName}">
		<aui:validator name="required" errorMessage="Student_Name_is_Mandatory" />
	</aui:input>
	
	<fmt:formatDate pattern="dd-MM-yyyy" value="${studentInfo.dateOfBirth}" var="formatedDateOfBirth" />
	<aui:input name="dateOfBirth" label="DateOfBirth" cssClass="form-control datePick" type="text" placeholder="dd-mm-yyyy"	value="${formatedDateOfBirth}">	</aui:input>
	
	<aui:field-wrapper name="gender">
		<aui:input inlineLabel="right" type="radio" value="M" label="radio.label.male" name="gender" checked="${(studentInfo.gender eq 'M') or (studentInfo.gender eq null) ?true:false}" />
		<aui:input checked="${studentInfo.gender eq 'F'?true:false}" name="gender" inlineLabel="right" type="radio" value="F" label="radio.label.female" />
	</aui:field-wrapper>
	
	<aui:button-row>
		<aui:button name="addStudentInfo" type="submit"	value="${studentInfo.studentId eq null?'button.label.add-student-info':'button.label.update-student-info'}" />
	</aui:button-row>

</aui:form>