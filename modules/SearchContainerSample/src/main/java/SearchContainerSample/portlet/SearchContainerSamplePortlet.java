package SearchContainerSample.portlet;

import SearchContainerSample.constants.SearchContainerSamplePortletKeys;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;

import CustomerDetail.service.employeeLocalService;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author parth.radadiya
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + SearchContainerSamplePortletKeys.SearchContainerSample,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class SearchContainerSamplePortlet extends MVCPortlet {
	
	@Reference
	employeeLocalService employeelocalservice;
	
	private static final Log log = LogFactoryUtil.getLog(SearchContainerSamplePortlet.class);
	
	public void addEmployeeInfo(ActionRequest actionRequest, ActionResponse actionResponse) {
		log.info("################# Inside addStudentInfo #########################");
		try {
			actionResponse.setRenderParameter("jspPage", "/addUpdate.jsp");
		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
}