package TestCustomerForm.portlet;

import TestCustomerForm.constants.TestCustomerFormPortletKeys;

import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactoryUtil;
import com.liferay.portal.kernel.dao.orm.PropertyFactoryUtil;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.PortalClassLoaderUtil;

import CustomerDetail.model.customer;
import CustomerDetail.service.customerLocalServiceUtil;

import java.io.IOException;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + TestCustomerFormPortletKeys.TestCustomerForm,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)

public class TestCustomerFormPortlet extends MVCPortlet {
	private static final Log LOG = LogFactoryUtil.getLog(TestCustomerFormPortlet.class);

	@Override
	public void serveResource(ResourceRequest resourceRequest, ResourceResponse resourceResponse)
			throws IOException, PortletException {
		LOG.info("ServeResource called");
		super.serveResource(resourceRequest, resourceResponse);
	}
	
	public void ActionFilter(ActionRequest req,ActionResponse res)throws Exception {
		LOG.info("ActionFilter called");
	}
	@Override
		public void doView(RenderRequest renderRequest, RenderResponse renderResponse)
				throws IOException, PortletException {
		DynamicQuery dynamic = DynamicQueryFactoryUtil.forClass(customer.class,"cu",PortalClassLoaderUtil.getClassLoader());
		dynamic.add(PropertyFactoryUtil.forName("cu.meal_choice").eq(new String("Vegetarian")));
		List<customer> dynamiccustomer = customerLocalServiceUtil.dynamicQuery(dynamic);
		LOG.info("dynamic search list : " + dynamiccustomer);
			super.doView(renderRequest, renderResponse);
		}
}