<%@ include file="/init.jsp"%>
<%@page import="CustomerDetail.service.customerLocalServiceUtil"%>
<%@page import="CustomerDetail.model.customer"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<style>
#th1 {
	background-color: lightgrey;
}

#th2 {
	background-color: lightblue;
}

#th3 {
	background-color: lightgreen;
}

#th4 {
	background-color: lightpink;
}

#th5 {
	background-color: lightyellow;
}
</style>

<portlet:renderURL var="ViewURL">
	<portlet:param name="mvcPath" value="/viewcustomer.jsp"></portlet:param>
</portlet:renderURL>

<aui:button-row>
	<aui:button class="btn btn-danger" onClick="${ViewURL.toString()}"
		value="Back"></aui:button>
</aui:button-row>
<div class="table-responsive">
	<table class="table table-bordered">
		<thead>

			<tr>
				<th id="th1">First_Name</th>
				<th id="th2">Last_Name</th>
				<th id="th3">Middel_Name</th>
				<th id="th4">Email_id</th>
				<th id="th5">Phone</th>
				<th id="th1">Date of Birth</th>
				<th id="th2">Age</th>
				<th id="th3">Gender</th>
				<th id="th4">Language</th>
				<th id="th5">Address</th>
				<th id="th1">Country</th>
				<th id="th2">State</th>
				<th id="th3">City</th>
				<th id="th4">Meal_Choice</th>
				<th id="th5">Meal_Preference</th>
				<th id="th1">Customer_Code</th>
				<th id="th2">Action</th>
			</tr>
			<%
					List<customer> custom = (List<customer>) request.getAttribute("customer");
					for (customer view : custom) {
				%>
		</thead>
		<tbody>
			<tr>
				<td><%=view.getFname()%></td>
				<td><%=view.getLname()%></td>
				<td><%=view.getMname()%></td>
				<td><%=view.getEmai_id()%></td>
				<td><%=view.getPhone()%></td>
				<td><%=view.getBirthdate()%></td>
				<td><%=view.getAge()%></td>
				<td><%=view.getGender()%></td>
				<td><%=view.getLanguage()%></td>
				<td><%=view.getAddress()%></td>
				<td><%=view.getCountry()%></td>
				<td><%=view.getState()%></td>
				<td><%=view.getCity()%></td>
				<td><%=view.getMeal_choice()%></td>
				<td><%=view.getMeal_preference()%></td>
				<td><%=view.getCutomercode()%></td>

				<td><portlet:renderURL var="UpdateURL">
						<portlet:param name="mvcPath" value="/UpdateCustomer.jsp"></portlet:param>
						<portlet:param name="Id" value="<%=String.valueOf(view.getId())%>" />
					</portlet:renderURL> <aui:button-row>
						<aui:button class="btn btn-success"
							onClick="${UpdateURL.toString()}" value="Update"></aui:button>
					</aui:button-row> <portlet:actionURL name="delete" var="actiondeleteURL">
						<portlet:param name="mvcPath" value="/viewcustomer.jsp"></portlet:param>
						<portlet:param name="Id" value="<%=String.valueOf(view.getId())%>" />
					</portlet:actionURL> <a href="${actiondeleteURL.toString()}">Delete</a></td>


				<!-- <aui:button-row>
							<aui:button class="btn btn-success"
								onClick="${DeleteURL.toString()}" value="Delete"></aui:button>
						</aui:button-row></td> -->
			</tr>
			<%
					}
				%>
		</tbody>

	</table>
</div>
