<%@ include file="/init.jsp"%>
<%@page import="com.liferay.portal.kernel.util.ParamUtil"%>
<%@page import="CustomerDetail.service.customerLocalServiceUtil"%>
<%@page import="CustomerDetail.model.customer"%>
<%@page import="java.util.List"%>
<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>

<c:set var="cobj" value="<%=request.getAttribute("cobj") %>" />
<fmt:formatDate pattern="yyyy-MM-dd" value="${cobj.birthdate}"
	var="Dateofbirth" />
<script>
	$("#country").change(function() {
		$("#state").empty();
		$("#city").empty();
		$("#state").append(new Option("-- Select a State --", null));
		for ( var state in countryData[$(this).val()]) {
			$("#state").append(new Option(state, state));
		}
	});
	$("#state").change(function() {
		var cities = countryData[$("#country").val()][$(this).val()];
		$("#city").empty();
		$("#city").append(new Option("-- Select a City --", null));
		for ( var city in cities) {
			$("#city").append(new Option(cities[city], cities[city]));
		}
	});
	var countryData = {
		"India" : {
			"Gujarat" : [ "Ahmedabad", "Surat", "Gandhinagar" ],
			"Maharashtra" : [ "Mumbai", "Pune" ],
			"Uttar Pradesh" : [ "Delhi", "Kanpur" ]
		},
		"America" : {
			"California" : [ "Los Angeles", "San Francisco", "San Diego" ],
			"Texas" : [ "Houston", "Dallas" ],
			"Florida" : [ "Miami", "Orlando" ]
		},
		"Japan" : {
			"Hokkaido" : [ "Sapporo", "Hakodate", "Otaru" ],
			"Aomori" : [ "Hirosaki", "Towada" ],
			"Fukushima" : [ "Aizukawamatsu", "Koriyama" ]
		}
	};
	$(document).ready(function() {
		for ( var country in countryData) {
			$("#country").append(new Option(country, country));
		}
	});

	/*...............................................Meal......................................................... */

	var mealDatabase = {
		"Vegetarian" : [ "Veg Biryani", "Pani Puri", "Dosa", "Samosa" ],
		"Non Vegetarian" : [ "Chicken Biryani", "Half Fry", "Boiled Eggs" ],
		"Any" : [ "Veg Biryani", "Pani Puri", "Dosa", "Samosa",
				"Chicken Biryani", "Half Fry", "Boiled Eggs" ]
	};
	$(document).ready(function() {
		for ( var mealChoice in mealDatabase) {
			$("#meal-choices").append(new Option(mealChoice, mealChoice));
		}
	});
	$("#meal-choices").change(function() {
		$("#meal-prefs").empty();
		$("#meal-prefs").append($("<option>", {
			value : null,
			text : "-- Choose What You Prefer --",
			disabled : true,
			selected : true
		}));
		var meals = mealDatabase[$(this).val()];
		for ( var meal in meals) {
			$("#meal-prefs").append(new Option(meals[meal], meals[meal]));
		}
	});

	<!-- /*.................................................CustomerRendomnumber.........................................*/
	-->

	(
			function() {
				function IDGenerator() {

					this.length = 4;
					this.timestamp = +new Date;

					var _getRandomInt = function(min, max) {
						return Math.floor(Math.random() * (max - min + 1))
								+ min;
					}

					this.generate = function() {
						var ts = this.timestamp.toString();
						var parts = ts.split("").reverse();
						var id = "";

						for (var i = 0; i < this.length; ++i) {
							var index = _getRandomInt(0, parts.length - 1);
							id += parts[index];
						}

						return id;
					}

				}

				document
						.addEventListener(
								"DOMContentLoaded",
								function() {
									var btn = document
											.querySelector("#generate"), output = document
											.querySelector("#output");

									btn.addEventListener("click", function() {
										var generator = new IDGenerator();
										output.value = generator.generate();

									}, false);

								});

			})();

	/* ...............................................I agree...................................................... */

	$(document).ready(function() {
		$('#myCheckbox').click(function() {
			$('#myButton').prop("disabled", !$("#myCheckbox").prop("checked"));
		})
	});
</script>

<portlet:actionURL name="UpateData" var="actionupdateURL">
	<portlet:param name="mvcPath" value="/view.jsp" />
	<%-- <c:if test="${not empty cobj}">
	<portlet:param name="Id" value="${cobj.Id}"/>
	</c:if> --%>
</portlet:actionURL>
<div class="text-center">
	<h1 class="text-info">
		<b><i>U</i></b><sub><u>pdate</u></sub> <b><i>R</i></b><sub><u>egistartion</u></sub>
		<b><i>F</i></b><sub><u>orm</u></sub>
	</h1>
</div>
<br>
<form action="${actionupdateURL}" method="post">
	<input name="<portlet:namespace/>Id" value="${cobj.getId()}" type="hidden" />  
	<div class="row">
		<div class="col-sm-4">
			<div class=form-group>
				<label>First Name * :</label> <input type="text"
					name="<portlet:namespace/>fname" value="${cobj.fname}"
					placeholder="First Name" class="form-control" required>
			</div>
		</div>
		<div class="col-sm-4">
			<div class=form-group>
				<label>Middel Name :</label> <input type="text"
					name="<portlet:namespace/>mname" value="${cobj.mname}"
					placeholder="Middel Name" class="form-control">
			</div>
		</div>

		<div class="col-sm-4">
			<div class=form-group>
				<label>Last Name :</label> <input type="text"
					name="<portlet:namespace/>lname" value="${cobj.lname}"
					placeholder="Last Name" class="form-control">
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-4">
			<label>Gender * :</label>
			<div class="form-group">
				<label for="male">Male</label> <input type="radio"
					name="<portlet:namespace/>gender" id="male" value="male"
					<c:if test="${fn:contains(cobj.gender,'male')}">checked</c:if>
					required>&nbsp;&nbsp; <label for="female">Female</label> <input
					type="radio" name="<portlet:namespace/>gender" id="female"
					value="female"
					<c:if test="${fn:contains(cobj.gender,'female')}">checked</c:if>
					required>&nbsp;&nbsp; <label for="other">Other</label> <input
					type="radio" name="<portlet:namespace/>gender" id="other"
					value="other"
					<c:if test="${fn:contains(cobj.gender,'other')}">checked</c:if>
					required>
			</div>
		</div>

		<div class="col-sm-4">
			<label>Languags :</label>
			<div class="form-group">
				<label>English</label> <input type="checkbox"
					name="<portlet:namespace/>language" value="English"
					<c:if test="${fn:contains(cobj.language,'English')}">checked</c:if>>
				&nbsp;&nbsp;<label>Gujarati</label> <input type="checkbox"
					name="<portlet:namespace/>language" value="Gujarati"
					<c:if test="${fn:contains(cobj.language,'Gujarati')}">checked</c:if>>
				&nbsp;&nbsp;<label>Hindi</label> <input type="checkbox"
					name="<portlet:namespace/>language" value="Hindi"
					<c:if test="${fn:contains(cobj.language,'Hindi')}">checked</c:if>>
			</div>
		</div>

		<div class="col-sm-4">
			<label>Phone Number :</label>
			<div class="form-group">
				<input type="tel" name="<portlet:namespace/>phone"
					value="${ cobj.phone}">
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-4">
			<label for="email">Email address *</label>
			<div class="form-group">
				<input type="email" name="<portlet:namespace/>email"
					class="form-control" value="${cobj.emai_id}" placeholder="Email Id"
					required>
			</div>
		</div>
		<div class="col-sm-4">
			<div class="form-group">
				<label>Date of Birth :</label> <input type="date"
					name="<portlet:namespace/>birthdate" value="${Dateofbirth}"
					class="form-control" required>
			</div>
		</div>

		<div class="col-sm-4">
			<div class="form-group">
				<label>Age :</label> <input type="text"
					name="<portlet:namespace/>age" value="${cobj.age }"
					class="form-control" placeholder="Enter Your age" min="18" required>
			</div>
		</div>
	</div>
	<br>
	<div class="container-fluid">
		<div class="row">
			<label>Address :</label>
			<div class="for-group">
				<textarea name="<portlet:namespace/>address"
					value="${cobj.address }" class="form-control" required>${cobj.address }"</textarea>
			</div>
		</div>
	</div>
	<br>

	<div class="row">
		<div class="form-group col-sm-4">
			<label for="country">Country</label> <select
				name="<portlet:namespace/>country" id="country" class="form-control"
				required>
				<c:if test="${not empty cobj}">
					<option value="${ cobj.country}">${cobj.country}</option>
				</c:if>
			</select>
		</div>
		<div class="form-group col-sm-4">
			<label for="state">State</label> <select
				name="<portlet:namespace/>state" id="state" class="form-control"
				required>
				<c:if test="${not empty cobj}">
					<option value="${ cobj.state}">${cobj.state}</option>
				</c:if>
			</select>
		</div>
		<div class="form-group col-sm-4">
			<label for="city">City</label> <select
				name="<portlet:namespace/>city" id="city" class="form-control"
				required>
				<c:if test="${not empty cobj}">
					<option value="${ cobj.city}">${cobj.city}</option>
				</c:if>
			</select>
		</div>
	</div>
	<br>
	<div class="row">
		<div class="col-sm-6">
			<label for="meal-choices">Meal Choice</label>
			<div class="form-group">
				<select name="<portlet:namespace/>meal-choice" id="meal-choices"
					 class="form-control">
					<c:if test="${not empty cobj}">
						<option values="${cobj.meal_choice}">${cobj.meal_choice}</option>
					</c:if>
				</select>
			</div>
		</div>
		<div class="col-sm-6">
			<label for="meal-prefs">Meal Preference</label>
			<div class="form-group">
				<select name="<portlet:namespace/>meal-preference" id="meal-prefs"
					 class="form-control">
					<c:if test="${not empty cobj}">
						<option values="${cobj.meal_preference}">${cobj.meal_preference}</option>
					</c:if>
				</select>
			</div>
		</div>
	</div>
	<br>
	<div class="row">
		<!-- <div class="form-group col-sm-8">
			<label>Customer Code</label> <input type="text" id="output"
				class="form-control"><input type="button" name="submit"
				value="Generate" id="generate">
		</div>
 -->
		<!-- <div class="col-sm-4"> -->
		<label>Enter Customer Code :</label>
		<div class="form-group">
			<input type="text" class="form-control"
				name="<portlet:namespace/>customercode"
				value="${cobj.cutomercode }" placeholder="enter your code"
				required>
			<!-- </div> -->
		</div>
	</div>
	<br>
	<div class="row">
		<div class="text-center">
			<div class="form-group">
				<label class="text-warning">I Agree :</label> <input id="myCheckbox"
					type="checkbox" id="Confirmation" name="agree" value="I Agree"
					required>
			</div>
		</div>
	</div>
	<br>
	<div class="container">
		<div class="row">
			<div class="text-left">
				<div class="form-group col-sm-6">
					<input id="myButton" type="submit" class="btn-success" id="Submit"
						name="submit" value="Submit" disabled>
				</div>
			</div>
			<div class="text-right">
				<div class="form-group col-sm-6">
					<input type="button" class="btn-danger" name="reset" value="Reset">
				</div>
			</div>
		</div>
	</div>