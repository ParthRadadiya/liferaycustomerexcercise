  <script>
      var countryData = {
        "India": {
          "Gujarat": ["Ahmedabad", "Surat", "Gandhinagar"],
          "Maharashtra": ["Mumbai", "Pune"],
          "Uttar Pradesh": ["Delhi", "Kanpur"]
        },
        "America": {
          "California": ["Los Angeles", "San Francisco", "San Diego"],
          "Texas": ["Houston", "Dallas"],
          "Florida": ["Miami", "Orlando"]
        },
        "Japan": {
          "Hokkaido": ["Sapporo", "Hakodate", "Otaru"],
          "Aomori": ["Hirosaki", "Towada"],
          "Fukushima": ["Aizukawamatsu", "Koriyama"]
        }
      };
      $(document).ready(function() {
        for(var country in countryData) {
          $("#country").append(new Option(country, country));
        }
      });
      $("#country").change(function() {
        $("#state").empty();
        $("#city").empty();
        $("#state").append(new Option("-- Select a State --", null));
        for(var state in countryData[$(this).val()]) {
          $("#state").append(new Option(state, state));
        }
      });
      $("#state").change(function() {
        var cities = countryData[$("#country").val()][$(this).val()];
        $("#city").empty();
        $("#city").append(new Option("-- Select a City --", null));
        for(var city in cities) {
          $("#city").append(new Option(cities[city], cities[city]));
        }
      });
      
 /*.................................................CustomerRendomnumber.........................................
      
      (function() {
    		 function IDGenerator() {
    		 
    			 this.length = 4;
    			 this.timestamp = +new Date;
    			 
    			 var _getRandomInt = function( min, max ) {
    				return Math.floor( Math.random() * ( max - min + 1 ) ) + min;
    			 }
    			 
    			 this.generate = function() {
    				 var ts = this.timestamp.toString();
    				 var parts = ts.split( "" ).reverse();
    				 var id = "";
    				 
    				 for( var i = 0; i < this.length; ++i ) {
    					var index = _getRandomInt( 0, parts.length - 1 );
    					id += parts[index];	 
    				 }
    				 
    				 return id;
    			 }

    			 
    		 }*/
    		 
    		 
    		 /*document.addEventListener( "DOMContentLoaded", function() {
    			var btn = document.querySelector( "#generate" ),
    				output = document.querySelector( "#output" );
    				
    			btn.addEventListener( "click", function() {
    				var generator = new IDGenerator();
    				output.value = generator.generate();
    				
    			}, false); 
    			 
    		 });
    		 
    		 
    	 })();

      $(document).ready(function(e) {
  		$("#Confirmation").click(function() {
  			$("#Submit").attr("disabled", !this.checked);
  		});
  	});*/
      
    </script>