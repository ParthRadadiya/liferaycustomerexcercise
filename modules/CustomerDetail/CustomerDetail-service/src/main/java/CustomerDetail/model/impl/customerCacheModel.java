/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package CustomerDetail.model.impl;

import CustomerDetail.model.customer;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.util.HashUtil;
import com.liferay.portal.kernel.util.StringBundler;

import java.io.Externalizable;
import java.io.IOException;
import java.io.ObjectInput;
import java.io.ObjectOutput;

import java.util.Date;

/**
 * The cache model class for representing customer in entity cache.
 *
 * @author Brian Wing Shun Chan
 * @see customer
 * @generated
 */
@ProviderType
public class customerCacheModel implements CacheModel<customer>, Externalizable {
	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof customerCacheModel)) {
			return false;
		}

		customerCacheModel customerCacheModel = (customerCacheModel)obj;

		if (Id == customerCacheModel.Id) {
			return true;
		}

		return false;
	}

	@Override
	public int hashCode() {
		return HashUtil.hash(0, Id);
	}

	@Override
	public String toString() {
		StringBundler sb = new StringBundler(35);

		sb.append("{Id=");
		sb.append(Id);
		sb.append(", fname=");
		sb.append(fname);
		sb.append(", lname=");
		sb.append(lname);
		sb.append(", mname=");
		sb.append(mname);
		sb.append(", age=");
		sb.append(age);
		sb.append(", birthdate=");
		sb.append(birthdate);
		sb.append(", phone=");
		sb.append(phone);
		sb.append(", cutomercode=");
		sb.append(cutomercode);
		sb.append(", emai_id=");
		sb.append(emai_id);
		sb.append(", gender=");
		sb.append(gender);
		sb.append(", address=");
		sb.append(address);
		sb.append(", language=");
		sb.append(language);
		sb.append(", country=");
		sb.append(country);
		sb.append(", state=");
		sb.append(state);
		sb.append(", city=");
		sb.append(city);
		sb.append(", meal_choice=");
		sb.append(meal_choice);
		sb.append(", meal_preference=");
		sb.append(meal_preference);
		sb.append("}");

		return sb.toString();
	}

	@Override
	public customer toEntityModel() {
		customerImpl customerImpl = new customerImpl();

		customerImpl.setId(Id);

		if (fname == null) {
			customerImpl.setFname("");
		}
		else {
			customerImpl.setFname(fname);
		}

		if (lname == null) {
			customerImpl.setLname("");
		}
		else {
			customerImpl.setLname(lname);
		}

		if (mname == null) {
			customerImpl.setMname("");
		}
		else {
			customerImpl.setMname(mname);
		}

		customerImpl.setAge(age);

		if (birthdate == Long.MIN_VALUE) {
			customerImpl.setBirthdate(null);
		}
		else {
			customerImpl.setBirthdate(new Date(birthdate));
		}

		customerImpl.setPhone(phone);

		if (cutomercode == null) {
			customerImpl.setCutomercode("");
		}
		else {
			customerImpl.setCutomercode(cutomercode);
		}

		if (emai_id == null) {
			customerImpl.setEmai_id("");
		}
		else {
			customerImpl.setEmai_id(emai_id);
		}

		if (gender == null) {
			customerImpl.setGender("");
		}
		else {
			customerImpl.setGender(gender);
		}

		if (address == null) {
			customerImpl.setAddress("");
		}
		else {
			customerImpl.setAddress(address);
		}

		if (language == null) {
			customerImpl.setLanguage("");
		}
		else {
			customerImpl.setLanguage(language);
		}

		if (country == null) {
			customerImpl.setCountry("");
		}
		else {
			customerImpl.setCountry(country);
		}

		if (state == null) {
			customerImpl.setState("");
		}
		else {
			customerImpl.setState(state);
		}

		if (city == null) {
			customerImpl.setCity("");
		}
		else {
			customerImpl.setCity(city);
		}

		if (meal_choice == null) {
			customerImpl.setMeal_choice("");
		}
		else {
			customerImpl.setMeal_choice(meal_choice);
		}

		if (meal_preference == null) {
			customerImpl.setMeal_preference("");
		}
		else {
			customerImpl.setMeal_preference(meal_preference);
		}

		customerImpl.resetOriginalValues();

		return customerImpl;
	}

	@Override
	public void readExternal(ObjectInput objectInput) throws IOException {
		Id = objectInput.readInt();
		fname = objectInput.readUTF();
		lname = objectInput.readUTF();
		mname = objectInput.readUTF();

		age = objectInput.readInt();
		birthdate = objectInput.readLong();

		phone = objectInput.readInt();
		cutomercode = objectInput.readUTF();
		emai_id = objectInput.readUTF();
		gender = objectInput.readUTF();
		address = objectInput.readUTF();
		language = objectInput.readUTF();
		country = objectInput.readUTF();
		state = objectInput.readUTF();
		city = objectInput.readUTF();
		meal_choice = objectInput.readUTF();
		meal_preference = objectInput.readUTF();
	}

	@Override
	public void writeExternal(ObjectOutput objectOutput)
		throws IOException {
		objectOutput.writeInt(Id);

		if (fname == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(fname);
		}

		if (lname == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(lname);
		}

		if (mname == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(mname);
		}

		objectOutput.writeInt(age);
		objectOutput.writeLong(birthdate);

		objectOutput.writeInt(phone);

		if (cutomercode == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(cutomercode);
		}

		if (emai_id == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(emai_id);
		}

		if (gender == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(gender);
		}

		if (address == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(address);
		}

		if (language == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(language);
		}

		if (country == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(country);
		}

		if (state == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(state);
		}

		if (city == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(city);
		}

		if (meal_choice == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(meal_choice);
		}

		if (meal_preference == null) {
			objectOutput.writeUTF("");
		}
		else {
			objectOutput.writeUTF(meal_preference);
		}
	}

	public int Id;
	public String fname;
	public String lname;
	public String mname;
	public int age;
	public long birthdate;
	public int phone;
	public String cutomercode;
	public String emai_id;
	public String gender;
	public String address;
	public String language;
	public String country;
	public String state;
	public String city;
	public String meal_choice;
	public String meal_preference;
}