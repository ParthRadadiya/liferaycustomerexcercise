/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package CustomerDetail.service.persistence.impl;

import CustomerDetail.exception.NoSuchcustomerException;

import CustomerDetail.model.customer;

import CustomerDetail.model.impl.customerImpl;
import CustomerDetail.model.impl.customerModelImpl;

import CustomerDetail.service.persistence.customerPersistence;

import aQute.bnd.annotation.ProviderType;

import com.liferay.portal.kernel.dao.orm.EntityCache;
import com.liferay.portal.kernel.dao.orm.FinderCache;
import com.liferay.portal.kernel.dao.orm.FinderPath;
import com.liferay.portal.kernel.dao.orm.Query;
import com.liferay.portal.kernel.dao.orm.QueryPos;
import com.liferay.portal.kernel.dao.orm.QueryUtil;
import com.liferay.portal.kernel.dao.orm.Session;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.service.persistence.impl.BasePersistenceImpl;
import com.liferay.portal.kernel.util.OrderByComparator;
import com.liferay.portal.kernel.util.ProxyUtil;
import com.liferay.portal.kernel.util.SetUtil;
import com.liferay.portal.kernel.util.StringBundler;
import com.liferay.portal.spring.extender.service.ServiceReference;

import java.io.Serializable;

import java.lang.reflect.Field;
import java.lang.reflect.InvocationHandler;

import java.util.Collections;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Objects;
import java.util.Set;

/**
 * The persistence implementation for the customer service.
 *
 * <p>
 * Caching information and settings can be found in <code>portal.properties</code>
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see customerPersistence
 * @see CustomerDetail.service.persistence.customerUtil
 * @generated
 */
@ProviderType
public class customerPersistenceImpl extends BasePersistenceImpl<customer>
	implements customerPersistence {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this class directly. Always use {@link customerUtil} to access the customer persistence. Modify <code>service.xml</code> and rerun ServiceBuilder to regenerate this class.
	 */
	public static final String FINDER_CLASS_NAME_ENTITY = customerImpl.class.getName();
	public static final String FINDER_CLASS_NAME_LIST_WITH_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List1";
	public static final String FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION = FINDER_CLASS_NAME_ENTITY +
		".List2";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_ALL = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, customerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, customerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findAll", new String[0]);
	public static final FinderPath FINDER_PATH_COUNT_ALL = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countAll", new String[0]);
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_MEALCHOICE =
		new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, customerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByMealChoice",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MEALCHOICE =
		new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, customerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByMealChoice",
			new String[] { String.class.getName() },
			customerModelImpl.MEAL_CHOICE_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_MEALCHOICE = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByMealChoice",
			new String[] { String.class.getName() });

	/**
	 * Returns all the customers where meal_choice = &#63;.
	 *
	 * @param meal_choice the meal_choice
	 * @return the matching customers
	 */
	@Override
	public List<customer> findByMealChoice(String meal_choice) {
		return findByMealChoice(meal_choice, QueryUtil.ALL_POS,
			QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the customers where meal_choice = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param meal_choice the meal_choice
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @return the range of matching customers
	 */
	@Override
	public List<customer> findByMealChoice(String meal_choice, int start,
		int end) {
		return findByMealChoice(meal_choice, start, end, null);
	}

	/**
	 * Returns an ordered range of all the customers where meal_choice = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param meal_choice the meal_choice
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching customers
	 */
	@Override
	public List<customer> findByMealChoice(String meal_choice, int start,
		int end, OrderByComparator<customer> orderByComparator) {
		return findByMealChoice(meal_choice, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the customers where meal_choice = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param meal_choice the meal_choice
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching customers
	 */
	@Override
	public List<customer> findByMealChoice(String meal_choice, int start,
		int end, OrderByComparator<customer> orderByComparator,
		boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MEALCHOICE;
			finderArgs = new Object[] { meal_choice };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_MEALCHOICE;
			finderArgs = new Object[] { meal_choice, start, end, orderByComparator };
		}

		List<customer> list = null;

		if (retrieveFromCache) {
			list = (List<customer>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (customer customer : list) {
					if (!Objects.equals(meal_choice, customer.getMeal_choice())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CUSTOMER_WHERE);

			boolean bindMeal_choice = false;

			if (meal_choice == null) {
				query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_1);
			}
			else if (meal_choice.equals("")) {
				query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_3);
			}
			else {
				bindMeal_choice = true;

				query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(customerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMeal_choice) {
					qPos.add(meal_choice);
				}

				if (!pagination) {
					list = (List<customer>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<customer>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first customer in the ordered set where meal_choice = &#63;.
	 *
	 * @param meal_choice the meal_choice
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching customer
	 * @throws NoSuchcustomerException if a matching customer could not be found
	 */
	@Override
	public customer findByMealChoice_First(String meal_choice,
		OrderByComparator<customer> orderByComparator)
		throws NoSuchcustomerException {
		customer customer = fetchByMealChoice_First(meal_choice,
				orderByComparator);

		if (customer != null) {
			return customer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("meal_choice=");
		msg.append(meal_choice);

		msg.append("}");

		throw new NoSuchcustomerException(msg.toString());
	}

	/**
	 * Returns the first customer in the ordered set where meal_choice = &#63;.
	 *
	 * @param meal_choice the meal_choice
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching customer, or <code>null</code> if a matching customer could not be found
	 */
	@Override
	public customer fetchByMealChoice_First(String meal_choice,
		OrderByComparator<customer> orderByComparator) {
		List<customer> list = findByMealChoice(meal_choice, 0, 1,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last customer in the ordered set where meal_choice = &#63;.
	 *
	 * @param meal_choice the meal_choice
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching customer
	 * @throws NoSuchcustomerException if a matching customer could not be found
	 */
	@Override
	public customer findByMealChoice_Last(String meal_choice,
		OrderByComparator<customer> orderByComparator)
		throws NoSuchcustomerException {
		customer customer = fetchByMealChoice_Last(meal_choice,
				orderByComparator);

		if (customer != null) {
			return customer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("meal_choice=");
		msg.append(meal_choice);

		msg.append("}");

		throw new NoSuchcustomerException(msg.toString());
	}

	/**
	 * Returns the last customer in the ordered set where meal_choice = &#63;.
	 *
	 * @param meal_choice the meal_choice
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching customer, or <code>null</code> if a matching customer could not be found
	 */
	@Override
	public customer fetchByMealChoice_Last(String meal_choice,
		OrderByComparator<customer> orderByComparator) {
		int count = countByMealChoice(meal_choice);

		if (count == 0) {
			return null;
		}

		List<customer> list = findByMealChoice(meal_choice, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the customers before and after the current customer in the ordered set where meal_choice = &#63;.
	 *
	 * @param Id the primary key of the current customer
	 * @param meal_choice the meal_choice
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next customer
	 * @throws NoSuchcustomerException if a customer with the primary key could not be found
	 */
	@Override
	public customer[] findByMealChoice_PrevAndNext(int Id, String meal_choice,
		OrderByComparator<customer> orderByComparator)
		throws NoSuchcustomerException {
		customer customer = findByPrimaryKey(Id);

		Session session = null;

		try {
			session = openSession();

			customer[] array = new customerImpl[3];

			array[0] = getByMealChoice_PrevAndNext(session, customer,
					meal_choice, orderByComparator, true);

			array[1] = customer;

			array[2] = getByMealChoice_PrevAndNext(session, customer,
					meal_choice, orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected customer getByMealChoice_PrevAndNext(Session session,
		customer customer, String meal_choice,
		OrderByComparator<customer> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CUSTOMER_WHERE);

		boolean bindMeal_choice = false;

		if (meal_choice == null) {
			query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_1);
		}
		else if (meal_choice.equals("")) {
			query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_3);
		}
		else {
			bindMeal_choice = true;

			query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(customerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindMeal_choice) {
			qPos.add(meal_choice);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(customer);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<customer> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the customers where meal_choice = &#63; from the database.
	 *
	 * @param meal_choice the meal_choice
	 */
	@Override
	public void removeByMealChoice(String meal_choice) {
		for (customer customer : findByMealChoice(meal_choice,
				QueryUtil.ALL_POS, QueryUtil.ALL_POS, null)) {
			remove(customer);
		}
	}

	/**
	 * Returns the number of customers where meal_choice = &#63;.
	 *
	 * @param meal_choice the meal_choice
	 * @return the number of matching customers
	 */
	@Override
	public int countByMealChoice(String meal_choice) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_MEALCHOICE;

		Object[] finderArgs = new Object[] { meal_choice };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CUSTOMER_WHERE);

			boolean bindMeal_choice = false;

			if (meal_choice == null) {
				query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_1);
			}
			else if (meal_choice.equals("")) {
				query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_3);
			}
			else {
				bindMeal_choice = true;

				query.append(_FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindMeal_choice) {
					qPos.add(meal_choice);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_1 = "customer.meal_choice IS NULL";
	private static final String _FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_2 = "customer.meal_choice = ?";
	private static final String _FINDER_COLUMN_MEALCHOICE_MEAL_CHOICE_3 = "(customer.meal_choice IS NULL OR customer.meal_choice = '')";
	public static final FinderPath FINDER_PATH_WITH_PAGINATION_FIND_BY_FNAME = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, customerImpl.class,
			FINDER_CLASS_NAME_LIST_WITH_PAGINATION, "findByfname",
			new String[] {
				String.class.getName(),
				
			Integer.class.getName(), Integer.class.getName(),
				OrderByComparator.class.getName()
			});
	public static final FinderPath FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FNAME = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, customerImpl.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "findByfname",
			new String[] { String.class.getName() },
			customerModelImpl.FNAME_COLUMN_BITMASK);
	public static final FinderPath FINDER_PATH_COUNT_BY_FNAME = new FinderPath(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerModelImpl.FINDER_CACHE_ENABLED, Long.class,
			FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION, "countByfname",
			new String[] { String.class.getName() });

	/**
	 * Returns all the customers where fname = &#63;.
	 *
	 * @param fname the fname
	 * @return the matching customers
	 */
	@Override
	public List<customer> findByfname(String fname) {
		return findByfname(fname, QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the customers where fname = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param fname the fname
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @return the range of matching customers
	 */
	@Override
	public List<customer> findByfname(String fname, int start, int end) {
		return findByfname(fname, start, end, null);
	}

	/**
	 * Returns an ordered range of all the customers where fname = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param fname the fname
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of matching customers
	 */
	@Override
	public List<customer> findByfname(String fname, int start, int end,
		OrderByComparator<customer> orderByComparator) {
		return findByfname(fname, start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the customers where fname = &#63;.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param fname the fname
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of matching customers
	 */
	@Override
	public List<customer> findByfname(String fname, int start, int end,
		OrderByComparator<customer> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FNAME;
			finderArgs = new Object[] { fname };
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_BY_FNAME;
			finderArgs = new Object[] { fname, start, end, orderByComparator };
		}

		List<customer> list = null;

		if (retrieveFromCache) {
			list = (List<customer>)finderCache.getResult(finderPath,
					finderArgs, this);

			if ((list != null) && !list.isEmpty()) {
				for (customer customer : list) {
					if (!Objects.equals(fname, customer.getFname())) {
						list = null;

						break;
					}
				}
			}
		}

		if (list == null) {
			StringBundler query = null;

			if (orderByComparator != null) {
				query = new StringBundler(3 +
						(orderByComparator.getOrderByFields().length * 2));
			}
			else {
				query = new StringBundler(3);
			}

			query.append(_SQL_SELECT_CUSTOMER_WHERE);

			boolean bindFname = false;

			if (fname == null) {
				query.append(_FINDER_COLUMN_FNAME_FNAME_1);
			}
			else if (fname.equals("")) {
				query.append(_FINDER_COLUMN_FNAME_FNAME_3);
			}
			else {
				bindFname = true;

				query.append(_FINDER_COLUMN_FNAME_FNAME_2);
			}

			if (orderByComparator != null) {
				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);
			}
			else
			 if (pagination) {
				query.append(customerModelImpl.ORDER_BY_JPQL);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindFname) {
					qPos.add(fname);
				}

				if (!pagination) {
					list = (List<customer>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<customer>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Returns the first customer in the ordered set where fname = &#63;.
	 *
	 * @param fname the fname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching customer
	 * @throws NoSuchcustomerException if a matching customer could not be found
	 */
	@Override
	public customer findByfname_First(String fname,
		OrderByComparator<customer> orderByComparator)
		throws NoSuchcustomerException {
		customer customer = fetchByfname_First(fname, orderByComparator);

		if (customer != null) {
			return customer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fname=");
		msg.append(fname);

		msg.append("}");

		throw new NoSuchcustomerException(msg.toString());
	}

	/**
	 * Returns the first customer in the ordered set where fname = &#63;.
	 *
	 * @param fname the fname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the first matching customer, or <code>null</code> if a matching customer could not be found
	 */
	@Override
	public customer fetchByfname_First(String fname,
		OrderByComparator<customer> orderByComparator) {
		List<customer> list = findByfname(fname, 0, 1, orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the last customer in the ordered set where fname = &#63;.
	 *
	 * @param fname the fname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching customer
	 * @throws NoSuchcustomerException if a matching customer could not be found
	 */
	@Override
	public customer findByfname_Last(String fname,
		OrderByComparator<customer> orderByComparator)
		throws NoSuchcustomerException {
		customer customer = fetchByfname_Last(fname, orderByComparator);

		if (customer != null) {
			return customer;
		}

		StringBundler msg = new StringBundler(4);

		msg.append(_NO_SUCH_ENTITY_WITH_KEY);

		msg.append("fname=");
		msg.append(fname);

		msg.append("}");

		throw new NoSuchcustomerException(msg.toString());
	}

	/**
	 * Returns the last customer in the ordered set where fname = &#63;.
	 *
	 * @param fname the fname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the last matching customer, or <code>null</code> if a matching customer could not be found
	 */
	@Override
	public customer fetchByfname_Last(String fname,
		OrderByComparator<customer> orderByComparator) {
		int count = countByfname(fname);

		if (count == 0) {
			return null;
		}

		List<customer> list = findByfname(fname, count - 1, count,
				orderByComparator);

		if (!list.isEmpty()) {
			return list.get(0);
		}

		return null;
	}

	/**
	 * Returns the customers before and after the current customer in the ordered set where fname = &#63;.
	 *
	 * @param Id the primary key of the current customer
	 * @param fname the fname
	 * @param orderByComparator the comparator to order the set by (optionally <code>null</code>)
	 * @return the previous, current, and next customer
	 * @throws NoSuchcustomerException if a customer with the primary key could not be found
	 */
	@Override
	public customer[] findByfname_PrevAndNext(int Id, String fname,
		OrderByComparator<customer> orderByComparator)
		throws NoSuchcustomerException {
		customer customer = findByPrimaryKey(Id);

		Session session = null;

		try {
			session = openSession();

			customer[] array = new customerImpl[3];

			array[0] = getByfname_PrevAndNext(session, customer, fname,
					orderByComparator, true);

			array[1] = customer;

			array[2] = getByfname_PrevAndNext(session, customer, fname,
					orderByComparator, false);

			return array;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	protected customer getByfname_PrevAndNext(Session session,
		customer customer, String fname,
		OrderByComparator<customer> orderByComparator, boolean previous) {
		StringBundler query = null;

		if (orderByComparator != null) {
			query = new StringBundler(4 +
					(orderByComparator.getOrderByConditionFields().length * 3) +
					(orderByComparator.getOrderByFields().length * 3));
		}
		else {
			query = new StringBundler(3);
		}

		query.append(_SQL_SELECT_CUSTOMER_WHERE);

		boolean bindFname = false;

		if (fname == null) {
			query.append(_FINDER_COLUMN_FNAME_FNAME_1);
		}
		else if (fname.equals("")) {
			query.append(_FINDER_COLUMN_FNAME_FNAME_3);
		}
		else {
			bindFname = true;

			query.append(_FINDER_COLUMN_FNAME_FNAME_2);
		}

		if (orderByComparator != null) {
			String[] orderByConditionFields = orderByComparator.getOrderByConditionFields();

			if (orderByConditionFields.length > 0) {
				query.append(WHERE_AND);
			}

			for (int i = 0; i < orderByConditionFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByConditionFields[i]);

				if ((i + 1) < orderByConditionFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN_HAS_NEXT);
					}
					else {
						query.append(WHERE_LESSER_THAN_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(WHERE_GREATER_THAN);
					}
					else {
						query.append(WHERE_LESSER_THAN);
					}
				}
			}

			query.append(ORDER_BY_CLAUSE);

			String[] orderByFields = orderByComparator.getOrderByFields();

			for (int i = 0; i < orderByFields.length; i++) {
				query.append(_ORDER_BY_ENTITY_ALIAS);
				query.append(orderByFields[i]);

				if ((i + 1) < orderByFields.length) {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC_HAS_NEXT);
					}
					else {
						query.append(ORDER_BY_DESC_HAS_NEXT);
					}
				}
				else {
					if (orderByComparator.isAscending() ^ previous) {
						query.append(ORDER_BY_ASC);
					}
					else {
						query.append(ORDER_BY_DESC);
					}
				}
			}
		}
		else {
			query.append(customerModelImpl.ORDER_BY_JPQL);
		}

		String sql = query.toString();

		Query q = session.createQuery(sql);

		q.setFirstResult(0);
		q.setMaxResults(2);

		QueryPos qPos = QueryPos.getInstance(q);

		if (bindFname) {
			qPos.add(fname);
		}

		if (orderByComparator != null) {
			Object[] values = orderByComparator.getOrderByConditionValues(customer);

			for (Object value : values) {
				qPos.add(value);
			}
		}

		List<customer> list = q.list();

		if (list.size() == 2) {
			return list.get(1);
		}
		else {
			return null;
		}
	}

	/**
	 * Removes all the customers where fname = &#63; from the database.
	 *
	 * @param fname the fname
	 */
	@Override
	public void removeByfname(String fname) {
		for (customer customer : findByfname(fname, QueryUtil.ALL_POS,
				QueryUtil.ALL_POS, null)) {
			remove(customer);
		}
	}

	/**
	 * Returns the number of customers where fname = &#63;.
	 *
	 * @param fname the fname
	 * @return the number of matching customers
	 */
	@Override
	public int countByfname(String fname) {
		FinderPath finderPath = FINDER_PATH_COUNT_BY_FNAME;

		Object[] finderArgs = new Object[] { fname };

		Long count = (Long)finderCache.getResult(finderPath, finderArgs, this);

		if (count == null) {
			StringBundler query = new StringBundler(2);

			query.append(_SQL_COUNT_CUSTOMER_WHERE);

			boolean bindFname = false;

			if (fname == null) {
				query.append(_FINDER_COLUMN_FNAME_FNAME_1);
			}
			else if (fname.equals("")) {
				query.append(_FINDER_COLUMN_FNAME_FNAME_3);
			}
			else {
				bindFname = true;

				query.append(_FINDER_COLUMN_FNAME_FNAME_2);
			}

			String sql = query.toString();

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				QueryPos qPos = QueryPos.getInstance(q);

				if (bindFname) {
					qPos.add(fname);
				}

				count = (Long)q.uniqueResult();

				finderCache.putResult(finderPath, finderArgs, count);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	private static final String _FINDER_COLUMN_FNAME_FNAME_1 = "customer.fname IS NULL";
	private static final String _FINDER_COLUMN_FNAME_FNAME_2 = "customer.fname = ?";
	private static final String _FINDER_COLUMN_FNAME_FNAME_3 = "(customer.fname IS NULL OR customer.fname = '')";

	public customerPersistenceImpl() {
		setModelClass(customer.class);

		try {
			Field field = BasePersistenceImpl.class.getDeclaredField(
					"_dbColumnNames");

			field.setAccessible(true);

			Map<String, String> dbColumnNames = new HashMap<String, String>();

			dbColumnNames.put("state", "state_");

			field.set(this, dbColumnNames);
		}
		catch (Exception e) {
			if (_log.isDebugEnabled()) {
				_log.debug(e, e);
			}
		}
	}

	/**
	 * Caches the customer in the entity cache if it is enabled.
	 *
	 * @param customer the customer
	 */
	@Override
	public void cacheResult(customer customer) {
		entityCache.putResult(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerImpl.class, customer.getPrimaryKey(), customer);

		customer.resetOriginalValues();
	}

	/**
	 * Caches the customers in the entity cache if it is enabled.
	 *
	 * @param customers the customers
	 */
	@Override
	public void cacheResult(List<customer> customers) {
		for (customer customer : customers) {
			if (entityCache.getResult(customerModelImpl.ENTITY_CACHE_ENABLED,
						customerImpl.class, customer.getPrimaryKey()) == null) {
				cacheResult(customer);
			}
			else {
				customer.resetOriginalValues();
			}
		}
	}

	/**
	 * Clears the cache for all customers.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache() {
		entityCache.clearCache(customerImpl.class);

		finderCache.clearCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	/**
	 * Clears the cache for the customer.
	 *
	 * <p>
	 * The {@link EntityCache} and {@link FinderCache} are both cleared by this method.
	 * </p>
	 */
	@Override
	public void clearCache(customer customer) {
		entityCache.removeResult(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerImpl.class, customer.getPrimaryKey());

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@Override
	public void clearCache(List<customer> customers) {
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);

		for (customer customer : customers) {
			entityCache.removeResult(customerModelImpl.ENTITY_CACHE_ENABLED,
				customerImpl.class, customer.getPrimaryKey());
		}
	}

	/**
	 * Creates a new customer with the primary key. Does not add the customer to the database.
	 *
	 * @param Id the primary key for the new customer
	 * @return the new customer
	 */
	@Override
	public customer create(int Id) {
		customer customer = new customerImpl();

		customer.setNew(true);
		customer.setPrimaryKey(Id);

		return customer;
	}

	/**
	 * Removes the customer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param Id the primary key of the customer
	 * @return the customer that was removed
	 * @throws NoSuchcustomerException if a customer with the primary key could not be found
	 */
	@Override
	public customer remove(int Id) throws NoSuchcustomerException {
		return remove((Serializable)Id);
	}

	/**
	 * Removes the customer with the primary key from the database. Also notifies the appropriate model listeners.
	 *
	 * @param primaryKey the primary key of the customer
	 * @return the customer that was removed
	 * @throws NoSuchcustomerException if a customer with the primary key could not be found
	 */
	@Override
	public customer remove(Serializable primaryKey)
		throws NoSuchcustomerException {
		Session session = null;

		try {
			session = openSession();

			customer customer = (customer)session.get(customerImpl.class,
					primaryKey);

			if (customer == null) {
				if (_log.isDebugEnabled()) {
					_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
				}

				throw new NoSuchcustomerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
					primaryKey);
			}

			return remove(customer);
		}
		catch (NoSuchcustomerException nsee) {
			throw nsee;
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}
	}

	@Override
	protected customer removeImpl(customer customer) {
		Session session = null;

		try {
			session = openSession();

			if (!session.contains(customer)) {
				customer = (customer)session.get(customerImpl.class,
						customer.getPrimaryKeyObj());
			}

			if (customer != null) {
				session.delete(customer);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		if (customer != null) {
			clearCache(customer);
		}

		return customer;
	}

	@Override
	public customer updateImpl(customer customer) {
		boolean isNew = customer.isNew();

		if (!(customer instanceof customerModelImpl)) {
			InvocationHandler invocationHandler = null;

			if (ProxyUtil.isProxyClass(customer.getClass())) {
				invocationHandler = ProxyUtil.getInvocationHandler(customer);

				throw new IllegalArgumentException(
					"Implement ModelWrapper in customer proxy " +
					invocationHandler.getClass());
			}

			throw new IllegalArgumentException(
				"Implement ModelWrapper in custom customer implementation " +
				customer.getClass());
		}

		customerModelImpl customerModelImpl = (customerModelImpl)customer;

		Session session = null;

		try {
			session = openSession();

			if (customer.isNew()) {
				session.save(customer);

				customer.setNew(false);
			}
			else {
				customer = (customer)session.merge(customer);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);

		if (!customerModelImpl.COLUMN_BITMASK_ENABLED) {
			finderCache.clearCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
		}
		else
		 if (isNew) {
			Object[] args = new Object[] { customerModelImpl.getMeal_choice() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_MEALCHOICE, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MEALCHOICE,
				args);

			args = new Object[] { customerModelImpl.getFname() };

			finderCache.removeResult(FINDER_PATH_COUNT_BY_FNAME, args);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FNAME,
				args);

			finderCache.removeResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY);
			finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL,
				FINDER_ARGS_EMPTY);
		}

		else {
			if ((customerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MEALCHOICE.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						customerModelImpl.getOriginalMeal_choice()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_MEALCHOICE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MEALCHOICE,
					args);

				args = new Object[] { customerModelImpl.getMeal_choice() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_MEALCHOICE, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_MEALCHOICE,
					args);
			}

			if ((customerModelImpl.getColumnBitmask() &
					FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FNAME.getColumnBitmask()) != 0) {
				Object[] args = new Object[] {
						customerModelImpl.getOriginalFname()
					};

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FNAME, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FNAME,
					args);

				args = new Object[] { customerModelImpl.getFname() };

				finderCache.removeResult(FINDER_PATH_COUNT_BY_FNAME, args);
				finderCache.removeResult(FINDER_PATH_WITHOUT_PAGINATION_FIND_BY_FNAME,
					args);
			}
		}

		entityCache.putResult(customerModelImpl.ENTITY_CACHE_ENABLED,
			customerImpl.class, customer.getPrimaryKey(), customer, false);

		customer.resetOriginalValues();

		return customer;
	}

	/**
	 * Returns the customer with the primary key or throws a {@link com.liferay.portal.kernel.exception.NoSuchModelException} if it could not be found.
	 *
	 * @param primaryKey the primary key of the customer
	 * @return the customer
	 * @throws NoSuchcustomerException if a customer with the primary key could not be found
	 */
	@Override
	public customer findByPrimaryKey(Serializable primaryKey)
		throws NoSuchcustomerException {
		customer customer = fetchByPrimaryKey(primaryKey);

		if (customer == null) {
			if (_log.isDebugEnabled()) {
				_log.debug(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY + primaryKey);
			}

			throw new NoSuchcustomerException(_NO_SUCH_ENTITY_WITH_PRIMARY_KEY +
				primaryKey);
		}

		return customer;
	}

	/**
	 * Returns the customer with the primary key or throws a {@link NoSuchcustomerException} if it could not be found.
	 *
	 * @param Id the primary key of the customer
	 * @return the customer
	 * @throws NoSuchcustomerException if a customer with the primary key could not be found
	 */
	@Override
	public customer findByPrimaryKey(int Id) throws NoSuchcustomerException {
		return findByPrimaryKey((Serializable)Id);
	}

	/**
	 * Returns the customer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param primaryKey the primary key of the customer
	 * @return the customer, or <code>null</code> if a customer with the primary key could not be found
	 */
	@Override
	public customer fetchByPrimaryKey(Serializable primaryKey) {
		Serializable serializable = entityCache.getResult(customerModelImpl.ENTITY_CACHE_ENABLED,
				customerImpl.class, primaryKey);

		if (serializable == nullModel) {
			return null;
		}

		customer customer = (customer)serializable;

		if (customer == null) {
			Session session = null;

			try {
				session = openSession();

				customer = (customer)session.get(customerImpl.class, primaryKey);

				if (customer != null) {
					cacheResult(customer);
				}
				else {
					entityCache.putResult(customerModelImpl.ENTITY_CACHE_ENABLED,
						customerImpl.class, primaryKey, nullModel);
				}
			}
			catch (Exception e) {
				entityCache.removeResult(customerModelImpl.ENTITY_CACHE_ENABLED,
					customerImpl.class, primaryKey);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return customer;
	}

	/**
	 * Returns the customer with the primary key or returns <code>null</code> if it could not be found.
	 *
	 * @param Id the primary key of the customer
	 * @return the customer, or <code>null</code> if a customer with the primary key could not be found
	 */
	@Override
	public customer fetchByPrimaryKey(int Id) {
		return fetchByPrimaryKey((Serializable)Id);
	}

	@Override
	public Map<Serializable, customer> fetchByPrimaryKeys(
		Set<Serializable> primaryKeys) {
		if (primaryKeys.isEmpty()) {
			return Collections.emptyMap();
		}

		Map<Serializable, customer> map = new HashMap<Serializable, customer>();

		if (primaryKeys.size() == 1) {
			Iterator<Serializable> iterator = primaryKeys.iterator();

			Serializable primaryKey = iterator.next();

			customer customer = fetchByPrimaryKey(primaryKey);

			if (customer != null) {
				map.put(primaryKey, customer);
			}

			return map;
		}

		Set<Serializable> uncachedPrimaryKeys = null;

		for (Serializable primaryKey : primaryKeys) {
			Serializable serializable = entityCache.getResult(customerModelImpl.ENTITY_CACHE_ENABLED,
					customerImpl.class, primaryKey);

			if (serializable != nullModel) {
				if (serializable == null) {
					if (uncachedPrimaryKeys == null) {
						uncachedPrimaryKeys = new HashSet<Serializable>();
					}

					uncachedPrimaryKeys.add(primaryKey);
				}
				else {
					map.put(primaryKey, (customer)serializable);
				}
			}
		}

		if (uncachedPrimaryKeys == null) {
			return map;
		}

		StringBundler query = new StringBundler((uncachedPrimaryKeys.size() * 2) +
				1);

		query.append(_SQL_SELECT_CUSTOMER_WHERE_PKS_IN);

		for (Serializable primaryKey : uncachedPrimaryKeys) {
			query.append((int)primaryKey);

			query.append(",");
		}

		query.setIndex(query.index() - 1);

		query.append(")");

		String sql = query.toString();

		Session session = null;

		try {
			session = openSession();

			Query q = session.createQuery(sql);

			for (customer customer : (List<customer>)q.list()) {
				map.put(customer.getPrimaryKeyObj(), customer);

				cacheResult(customer);

				uncachedPrimaryKeys.remove(customer.getPrimaryKeyObj());
			}

			for (Serializable primaryKey : uncachedPrimaryKeys) {
				entityCache.putResult(customerModelImpl.ENTITY_CACHE_ENABLED,
					customerImpl.class, primaryKey, nullModel);
			}
		}
		catch (Exception e) {
			throw processException(e);
		}
		finally {
			closeSession(session);
		}

		return map;
	}

	/**
	 * Returns all the customers.
	 *
	 * @return the customers
	 */
	@Override
	public List<customer> findAll() {
		return findAll(QueryUtil.ALL_POS, QueryUtil.ALL_POS, null);
	}

	/**
	 * Returns a range of all the customers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @return the range of customers
	 */
	@Override
	public List<customer> findAll(int start, int end) {
		return findAll(start, end, null);
	}

	/**
	 * Returns an ordered range of all the customers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @return the ordered range of customers
	 */
	@Override
	public List<customer> findAll(int start, int end,
		OrderByComparator<customer> orderByComparator) {
		return findAll(start, end, orderByComparator, true);
	}

	/**
	 * Returns an ordered range of all the customers.
	 *
	 * <p>
	 * Useful when paginating results. Returns a maximum of <code>end - start</code> instances. <code>start</code> and <code>end</code> are not primary keys, they are indexes in the result set. Thus, <code>0</code> refers to the first result in the set. Setting both <code>start</code> and <code>end</code> to {@link QueryUtil#ALL_POS} will return the full result set. If <code>orderByComparator</code> is specified, then the query will include the given ORDER BY logic. If <code>orderByComparator</code> is absent and pagination is required (<code>start</code> and <code>end</code> are not {@link QueryUtil#ALL_POS}), then the query will include the default ORDER BY logic from {@link customerModelImpl}. If both <code>orderByComparator</code> and pagination are absent, for performance reasons, the query will not have an ORDER BY clause and the returned result set will be sorted on by the primary key in an ascending order.
	 * </p>
	 *
	 * @param start the lower bound of the range of customers
	 * @param end the upper bound of the range of customers (not inclusive)
	 * @param orderByComparator the comparator to order the results by (optionally <code>null</code>)
	 * @param retrieveFromCache whether to retrieve from the finder cache
	 * @return the ordered range of customers
	 */
	@Override
	public List<customer> findAll(int start, int end,
		OrderByComparator<customer> orderByComparator, boolean retrieveFromCache) {
		boolean pagination = true;
		FinderPath finderPath = null;
		Object[] finderArgs = null;

		if ((start == QueryUtil.ALL_POS) && (end == QueryUtil.ALL_POS) &&
				(orderByComparator == null)) {
			pagination = false;
			finderPath = FINDER_PATH_WITHOUT_PAGINATION_FIND_ALL;
			finderArgs = FINDER_ARGS_EMPTY;
		}
		else {
			finderPath = FINDER_PATH_WITH_PAGINATION_FIND_ALL;
			finderArgs = new Object[] { start, end, orderByComparator };
		}

		List<customer> list = null;

		if (retrieveFromCache) {
			list = (List<customer>)finderCache.getResult(finderPath,
					finderArgs, this);
		}

		if (list == null) {
			StringBundler query = null;
			String sql = null;

			if (orderByComparator != null) {
				query = new StringBundler(2 +
						(orderByComparator.getOrderByFields().length * 2));

				query.append(_SQL_SELECT_CUSTOMER);

				appendOrderByComparator(query, _ORDER_BY_ENTITY_ALIAS,
					orderByComparator);

				sql = query.toString();
			}
			else {
				sql = _SQL_SELECT_CUSTOMER;

				if (pagination) {
					sql = sql.concat(customerModelImpl.ORDER_BY_JPQL);
				}
			}

			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(sql);

				if (!pagination) {
					list = (List<customer>)QueryUtil.list(q, getDialect(),
							start, end, false);

					Collections.sort(list);

					list = Collections.unmodifiableList(list);
				}
				else {
					list = (List<customer>)QueryUtil.list(q, getDialect(),
							start, end);
				}

				cacheResult(list);

				finderCache.putResult(finderPath, finderArgs, list);
			}
			catch (Exception e) {
				finderCache.removeResult(finderPath, finderArgs);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return list;
	}

	/**
	 * Removes all the customers from the database.
	 *
	 */
	@Override
	public void removeAll() {
		for (customer customer : findAll()) {
			remove(customer);
		}
	}

	/**
	 * Returns the number of customers.
	 *
	 * @return the number of customers
	 */
	@Override
	public int countAll() {
		Long count = (Long)finderCache.getResult(FINDER_PATH_COUNT_ALL,
				FINDER_ARGS_EMPTY, this);

		if (count == null) {
			Session session = null;

			try {
				session = openSession();

				Query q = session.createQuery(_SQL_COUNT_CUSTOMER);

				count = (Long)q.uniqueResult();

				finderCache.putResult(FINDER_PATH_COUNT_ALL, FINDER_ARGS_EMPTY,
					count);
			}
			catch (Exception e) {
				finderCache.removeResult(FINDER_PATH_COUNT_ALL,
					FINDER_ARGS_EMPTY);

				throw processException(e);
			}
			finally {
				closeSession(session);
			}
		}

		return count.intValue();
	}

	@Override
	public Set<String> getBadColumnNames() {
		return _badColumnNames;
	}

	@Override
	protected Map<String, Integer> getTableColumnsMap() {
		return customerModelImpl.TABLE_COLUMNS_MAP;
	}

	/**
	 * Initializes the customer persistence.
	 */
	public void afterPropertiesSet() {
	}

	public void destroy() {
		entityCache.removeCache(customerImpl.class.getName());
		finderCache.removeCache(FINDER_CLASS_NAME_ENTITY);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITH_PAGINATION);
		finderCache.removeCache(FINDER_CLASS_NAME_LIST_WITHOUT_PAGINATION);
	}

	@ServiceReference(type = EntityCache.class)
	protected EntityCache entityCache;
	@ServiceReference(type = FinderCache.class)
	protected FinderCache finderCache;
	private static final String _SQL_SELECT_CUSTOMER = "SELECT customer FROM customer customer";
	private static final String _SQL_SELECT_CUSTOMER_WHERE_PKS_IN = "SELECT customer FROM customer customer WHERE Id IN (";
	private static final String _SQL_SELECT_CUSTOMER_WHERE = "SELECT customer FROM customer customer WHERE ";
	private static final String _SQL_COUNT_CUSTOMER = "SELECT COUNT(customer) FROM customer customer";
	private static final String _SQL_COUNT_CUSTOMER_WHERE = "SELECT COUNT(customer) FROM customer customer WHERE ";
	private static final String _ORDER_BY_ENTITY_ALIAS = "customer.";
	private static final String _NO_SUCH_ENTITY_WITH_PRIMARY_KEY = "No customer exists with the primary key ";
	private static final String _NO_SUCH_ENTITY_WITH_KEY = "No customer exists with the key {";
	private static final Log _log = LogFactoryUtil.getLog(customerPersistenceImpl.class);
	private static final Set<String> _badColumnNames = SetUtil.fromArray(new String[] {
				"state"
			});
}