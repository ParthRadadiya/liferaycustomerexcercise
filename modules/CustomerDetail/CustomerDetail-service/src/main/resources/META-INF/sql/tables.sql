create table FOO_customer (
	Id INTEGER not null primary key,
	fname VARCHAR(75) null,
	lname VARCHAR(75) null,
	mname VARCHAR(75) null,
	age INTEGER,
	birthdate DATE null,
	phone INTEGER,
	cutomercode VARCHAR(75) null,
	emai_id VARCHAR(75) null,
	gender VARCHAR(75) null,
	address VARCHAR(75) null,
	language VARCHAR(75) null,
	country VARCHAR(75) null,
	state_ VARCHAR(75) null,
	city VARCHAR(75) null,
	meal_choice VARCHAR(75) null,
	meal_preference VARCHAR(75) null
);

create table FOO_employee (
	Id INTEGER not null primary key,
	name VARCHAR(75) null,
	password_ VARCHAR(75) null
);