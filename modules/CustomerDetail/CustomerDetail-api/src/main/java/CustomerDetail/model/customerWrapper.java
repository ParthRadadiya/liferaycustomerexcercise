/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package CustomerDetail.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.model.ModelWrapper;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.Objects;

/**
 * <p>
 * This class is a wrapper for {@link customer}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see customer
 * @generated
 */
@ProviderType
public class customerWrapper implements customer, ModelWrapper<customer> {
	public customerWrapper(customer customer) {
		_customer = customer;
	}

	@Override
	public Class<?> getModelClass() {
		return customer.class;
	}

	@Override
	public String getModelClassName() {
		return customer.class.getName();
	}

	@Override
	public Map<String, Object> getModelAttributes() {
		Map<String, Object> attributes = new HashMap<String, Object>();

		attributes.put("Id", getId());
		attributes.put("fname", getFname());
		attributes.put("lname", getLname());
		attributes.put("mname", getMname());
		attributes.put("age", getAge());
		attributes.put("birthdate", getBirthdate());
		attributes.put("phone", getPhone());
		attributes.put("cutomercode", getCutomercode());
		attributes.put("emai_id", getEmai_id());
		attributes.put("gender", getGender());
		attributes.put("address", getAddress());
		attributes.put("language", getLanguage());
		attributes.put("country", getCountry());
		attributes.put("state", getState());
		attributes.put("city", getCity());
		attributes.put("meal_choice", getMeal_choice());
		attributes.put("meal_preference", getMeal_preference());

		return attributes;
	}

	@Override
	public void setModelAttributes(Map<String, Object> attributes) {
		Integer Id = (Integer)attributes.get("Id");

		if (Id != null) {
			setId(Id);
		}

		String fname = (String)attributes.get("fname");

		if (fname != null) {
			setFname(fname);
		}

		String lname = (String)attributes.get("lname");

		if (lname != null) {
			setLname(lname);
		}

		String mname = (String)attributes.get("mname");

		if (mname != null) {
			setMname(mname);
		}

		Integer age = (Integer)attributes.get("age");

		if (age != null) {
			setAge(age);
		}

		Date birthdate = (Date)attributes.get("birthdate");

		if (birthdate != null) {
			setBirthdate(birthdate);
		}

		Integer phone = (Integer)attributes.get("phone");

		if (phone != null) {
			setPhone(phone);
		}

		String cutomercode = (String)attributes.get("cutomercode");

		if (cutomercode != null) {
			setCutomercode(cutomercode);
		}

		String emai_id = (String)attributes.get("emai_id");

		if (emai_id != null) {
			setEmai_id(emai_id);
		}

		String gender = (String)attributes.get("gender");

		if (gender != null) {
			setGender(gender);
		}

		String address = (String)attributes.get("address");

		if (address != null) {
			setAddress(address);
		}

		String language = (String)attributes.get("language");

		if (language != null) {
			setLanguage(language);
		}

		String country = (String)attributes.get("country");

		if (country != null) {
			setCountry(country);
		}

		String state = (String)attributes.get("state");

		if (state != null) {
			setState(state);
		}

		String city = (String)attributes.get("city");

		if (city != null) {
			setCity(city);
		}

		String meal_choice = (String)attributes.get("meal_choice");

		if (meal_choice != null) {
			setMeal_choice(meal_choice);
		}

		String meal_preference = (String)attributes.get("meal_preference");

		if (meal_preference != null) {
			setMeal_preference(meal_preference);
		}
	}

	@Override
	public Object clone() {
		return new customerWrapper((customer)_customer.clone());
	}

	@Override
	public int compareTo(CustomerDetail.model.customer customer) {
		return _customer.compareTo(customer);
	}

	/**
	* Returns the address of this customer.
	*
	* @return the address of this customer
	*/
	@Override
	public String getAddress() {
		return _customer.getAddress();
	}

	/**
	* Returns the age of this customer.
	*
	* @return the age of this customer
	*/
	@Override
	public int getAge() {
		return _customer.getAge();
	}

	/**
	* Returns the birthdate of this customer.
	*
	* @return the birthdate of this customer
	*/
	@Override
	public Date getBirthdate() {
		return _customer.getBirthdate();
	}

	/**
	* Returns the city of this customer.
	*
	* @return the city of this customer
	*/
	@Override
	public String getCity() {
		return _customer.getCity();
	}

	/**
	* Returns the country of this customer.
	*
	* @return the country of this customer
	*/
	@Override
	public String getCountry() {
		return _customer.getCountry();
	}

	/**
	* Returns the cutomercode of this customer.
	*
	* @return the cutomercode of this customer
	*/
	@Override
	public String getCutomercode() {
		return _customer.getCutomercode();
	}

	/**
	* Returns the emai_id of this customer.
	*
	* @return the emai_id of this customer
	*/
	@Override
	public String getEmai_id() {
		return _customer.getEmai_id();
	}

	@Override
	public ExpandoBridge getExpandoBridge() {
		return _customer.getExpandoBridge();
	}

	/**
	* Returns the fname of this customer.
	*
	* @return the fname of this customer
	*/
	@Override
	public String getFname() {
		return _customer.getFname();
	}

	/**
	* Returns the gender of this customer.
	*
	* @return the gender of this customer
	*/
	@Override
	public String getGender() {
		return _customer.getGender();
	}

	/**
	* Returns the ID of this customer.
	*
	* @return the ID of this customer
	*/
	@Override
	public int getId() {
		return _customer.getId();
	}

	/**
	* Returns the language of this customer.
	*
	* @return the language of this customer
	*/
	@Override
	public String getLanguage() {
		return _customer.getLanguage();
	}

	/**
	* Returns the lname of this customer.
	*
	* @return the lname of this customer
	*/
	@Override
	public String getLname() {
		return _customer.getLname();
	}

	/**
	* Returns the meal_choice of this customer.
	*
	* @return the meal_choice of this customer
	*/
	@Override
	public String getMeal_choice() {
		return _customer.getMeal_choice();
	}

	/**
	* Returns the meal_preference of this customer.
	*
	* @return the meal_preference of this customer
	*/
	@Override
	public String getMeal_preference() {
		return _customer.getMeal_preference();
	}

	/**
	* Returns the mname of this customer.
	*
	* @return the mname of this customer
	*/
	@Override
	public String getMname() {
		return _customer.getMname();
	}

	/**
	* Returns the phone of this customer.
	*
	* @return the phone of this customer
	*/
	@Override
	public int getPhone() {
		return _customer.getPhone();
	}

	/**
	* Returns the primary key of this customer.
	*
	* @return the primary key of this customer
	*/
	@Override
	public int getPrimaryKey() {
		return _customer.getPrimaryKey();
	}

	@Override
	public Serializable getPrimaryKeyObj() {
		return _customer.getPrimaryKeyObj();
	}

	/**
	* Returns the state of this customer.
	*
	* @return the state of this customer
	*/
	@Override
	public String getState() {
		return _customer.getState();
	}

	@Override
	public int hashCode() {
		return _customer.hashCode();
	}

	@Override
	public boolean isCachedModel() {
		return _customer.isCachedModel();
	}

	@Override
	public boolean isEscapedModel() {
		return _customer.isEscapedModel();
	}

	@Override
	public boolean isNew() {
		return _customer.isNew();
	}

	@Override
	public void persist() {
		_customer.persist();
	}

	/**
	* Sets the address of this customer.
	*
	* @param address the address of this customer
	*/
	@Override
	public void setAddress(String address) {
		_customer.setAddress(address);
	}

	/**
	* Sets the age of this customer.
	*
	* @param age the age of this customer
	*/
	@Override
	public void setAge(int age) {
		_customer.setAge(age);
	}

	/**
	* Sets the birthdate of this customer.
	*
	* @param birthdate the birthdate of this customer
	*/
	@Override
	public void setBirthdate(Date birthdate) {
		_customer.setBirthdate(birthdate);
	}

	@Override
	public void setCachedModel(boolean cachedModel) {
		_customer.setCachedModel(cachedModel);
	}

	/**
	* Sets the city of this customer.
	*
	* @param city the city of this customer
	*/
	@Override
	public void setCity(String city) {
		_customer.setCity(city);
	}

	/**
	* Sets the country of this customer.
	*
	* @param country the country of this customer
	*/
	@Override
	public void setCountry(String country) {
		_customer.setCountry(country);
	}

	/**
	* Sets the cutomercode of this customer.
	*
	* @param cutomercode the cutomercode of this customer
	*/
	@Override
	public void setCutomercode(String cutomercode) {
		_customer.setCutomercode(cutomercode);
	}

	/**
	* Sets the emai_id of this customer.
	*
	* @param emai_id the emai_id of this customer
	*/
	@Override
	public void setEmai_id(String emai_id) {
		_customer.setEmai_id(emai_id);
	}

	@Override
	public void setExpandoBridgeAttributes(
		com.liferay.portal.kernel.model.BaseModel<?> baseModel) {
		_customer.setExpandoBridgeAttributes(baseModel);
	}

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge) {
		_customer.setExpandoBridgeAttributes(expandoBridge);
	}

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext) {
		_customer.setExpandoBridgeAttributes(serviceContext);
	}

	/**
	* Sets the fname of this customer.
	*
	* @param fname the fname of this customer
	*/
	@Override
	public void setFname(String fname) {
		_customer.setFname(fname);
	}

	/**
	* Sets the gender of this customer.
	*
	* @param gender the gender of this customer
	*/
	@Override
	public void setGender(String gender) {
		_customer.setGender(gender);
	}

	/**
	* Sets the ID of this customer.
	*
	* @param Id the ID of this customer
	*/
	@Override
	public void setId(int Id) {
		_customer.setId(Id);
	}

	/**
	* Sets the language of this customer.
	*
	* @param language the language of this customer
	*/
	@Override
	public void setLanguage(String language) {
		_customer.setLanguage(language);
	}

	/**
	* Sets the lname of this customer.
	*
	* @param lname the lname of this customer
	*/
	@Override
	public void setLname(String lname) {
		_customer.setLname(lname);
	}

	/**
	* Sets the meal_choice of this customer.
	*
	* @param meal_choice the meal_choice of this customer
	*/
	@Override
	public void setMeal_choice(String meal_choice) {
		_customer.setMeal_choice(meal_choice);
	}

	/**
	* Sets the meal_preference of this customer.
	*
	* @param meal_preference the meal_preference of this customer
	*/
	@Override
	public void setMeal_preference(String meal_preference) {
		_customer.setMeal_preference(meal_preference);
	}

	/**
	* Sets the mname of this customer.
	*
	* @param mname the mname of this customer
	*/
	@Override
	public void setMname(String mname) {
		_customer.setMname(mname);
	}

	@Override
	public void setNew(boolean n) {
		_customer.setNew(n);
	}

	/**
	* Sets the phone of this customer.
	*
	* @param phone the phone of this customer
	*/
	@Override
	public void setPhone(int phone) {
		_customer.setPhone(phone);
	}

	/**
	* Sets the primary key of this customer.
	*
	* @param primaryKey the primary key of this customer
	*/
	@Override
	public void setPrimaryKey(int primaryKey) {
		_customer.setPrimaryKey(primaryKey);
	}

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj) {
		_customer.setPrimaryKeyObj(primaryKeyObj);
	}

	/**
	* Sets the state of this customer.
	*
	* @param state the state of this customer
	*/
	@Override
	public void setState(String state) {
		_customer.setState(state);
	}

	@Override
	public com.liferay.portal.kernel.model.CacheModel<CustomerDetail.model.customer> toCacheModel() {
		return _customer.toCacheModel();
	}

	@Override
	public CustomerDetail.model.customer toEscapedModel() {
		return new customerWrapper(_customer.toEscapedModel());
	}

	@Override
	public String toString() {
		return _customer.toString();
	}

	@Override
	public CustomerDetail.model.customer toUnescapedModel() {
		return new customerWrapper(_customer.toUnescapedModel());
	}

	@Override
	public String toXmlString() {
		return _customer.toXmlString();
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}

		if (!(obj instanceof customerWrapper)) {
			return false;
		}

		customerWrapper customerWrapper = (customerWrapper)obj;

		if (Objects.equals(_customer, customerWrapper._customer)) {
			return true;
		}

		return false;
	}

	@Override
	public customer getWrappedModel() {
		return _customer;
	}

	@Override
	public boolean isEntityCacheEnabled() {
		return _customer.isEntityCacheEnabled();
	}

	@Override
	public boolean isFinderCacheEnabled() {
		return _customer.isFinderCacheEnabled();
	}

	@Override
	public void resetOriginalValues() {
		_customer.resetOriginalValues();
	}

	private final customer _customer;
}