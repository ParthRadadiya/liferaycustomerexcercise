/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package CustomerDetail.model;

import aQute.bnd.annotation.ProviderType;

import com.liferay.expando.kernel.model.ExpandoBridge;

import com.liferay.portal.kernel.bean.AutoEscape;
import com.liferay.portal.kernel.model.BaseModel;
import com.liferay.portal.kernel.model.CacheModel;
import com.liferay.portal.kernel.service.ServiceContext;

import java.io.Serializable;

/**
 * The base model interface for the employee service. Represents a row in the &quot;FOO_employee&quot; database table, with each column mapped to a property of this class.
 *
 * <p>
 * This interface and its corresponding implementation {@link CustomerDetail.model.impl.employeeModelImpl} exist only as a container for the default property accessors generated by ServiceBuilder. Helper methods and all application logic should be put in {@link CustomerDetail.model.impl.employeeImpl}.
 * </p>
 *
 * @author Brian Wing Shun Chan
 * @see employee
 * @see CustomerDetail.model.impl.employeeImpl
 * @see CustomerDetail.model.impl.employeeModelImpl
 * @generated
 */
@ProviderType
public interface employeeModel extends BaseModel<employee> {
	/*
	 * NOTE FOR DEVELOPERS:
	 *
	 * Never modify or reference this interface directly. All methods that expect a employee model instance should use the {@link employee} interface instead.
	 */

	/**
	 * Returns the primary key of this employee.
	 *
	 * @return the primary key of this employee
	 */
	public int getPrimaryKey();

	/**
	 * Sets the primary key of this employee.
	 *
	 * @param primaryKey the primary key of this employee
	 */
	public void setPrimaryKey(int primaryKey);

	/**
	 * Returns the ID of this employee.
	 *
	 * @return the ID of this employee
	 */
	public int getId();

	/**
	 * Sets the ID of this employee.
	 *
	 * @param Id the ID of this employee
	 */
	public void setId(int Id);

	/**
	 * Returns the name of this employee.
	 *
	 * @return the name of this employee
	 */
	@AutoEscape
	public String getName();

	/**
	 * Sets the name of this employee.
	 *
	 * @param name the name of this employee
	 */
	public void setName(String name);

	/**
	 * Returns the password of this employee.
	 *
	 * @return the password of this employee
	 */
	@AutoEscape
	public String getPassword();

	/**
	 * Sets the password of this employee.
	 *
	 * @param password the password of this employee
	 */
	public void setPassword(String password);

	@Override
	public boolean isNew();

	@Override
	public void setNew(boolean n);

	@Override
	public boolean isCachedModel();

	@Override
	public void setCachedModel(boolean cachedModel);

	@Override
	public boolean isEscapedModel();

	@Override
	public Serializable getPrimaryKeyObj();

	@Override
	public void setPrimaryKeyObj(Serializable primaryKeyObj);

	@Override
	public ExpandoBridge getExpandoBridge();

	@Override
	public void setExpandoBridgeAttributes(BaseModel<?> baseModel);

	@Override
	public void setExpandoBridgeAttributes(ExpandoBridge expandoBridge);

	@Override
	public void setExpandoBridgeAttributes(ServiceContext serviceContext);

	@Override
	public Object clone();

	@Override
	public int compareTo(CustomerDetail.model.employee employee);

	@Override
	public int hashCode();

	@Override
	public CacheModel<CustomerDetail.model.employee> toCacheModel();

	@Override
	public CustomerDetail.model.employee toEscapedModel();

	@Override
	public CustomerDetail.model.employee toUnescapedModel();

	@Override
	public String toString();

	@Override
	public String toXmlString();
}