/**
 * Copyright (c) 2000-present Liferay, Inc. All rights reserved.
 *
 * This library is free software; you can redistribute it and/or modify it under
 * the terms of the GNU Lesser General Public License as published by the Free
 * Software Foundation; either version 2.1 of the License, or (at your option)
 * any later version.
 *
 * This library is distributed in the hope that it will be useful, but WITHOUT
 * ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS
 * FOR A PARTICULAR PURPOSE. See the GNU Lesser General Public License for more
 * details.
 */

package CustomerDetail.model;

import aQute.bnd.annotation.ProviderType;

import java.io.Serializable;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

/**
 * This class is used by SOAP remote services.
 *
 * @author Brian Wing Shun Chan
 * @generated
 */
@ProviderType
public class customerSoap implements Serializable {
	public static customerSoap toSoapModel(customer model) {
		customerSoap soapModel = new customerSoap();

		soapModel.setId(model.getId());
		soapModel.setFname(model.getFname());
		soapModel.setLname(model.getLname());
		soapModel.setMname(model.getMname());
		soapModel.setAge(model.getAge());
		soapModel.setBirthdate(model.getBirthdate());
		soapModel.setPhone(model.getPhone());
		soapModel.setCutomercode(model.getCutomercode());
		soapModel.setEmai_id(model.getEmai_id());
		soapModel.setGender(model.getGender());
		soapModel.setAddress(model.getAddress());
		soapModel.setLanguage(model.getLanguage());
		soapModel.setCountry(model.getCountry());
		soapModel.setState(model.getState());
		soapModel.setCity(model.getCity());
		soapModel.setMeal_choice(model.getMeal_choice());
		soapModel.setMeal_preference(model.getMeal_preference());

		return soapModel;
	}

	public static customerSoap[] toSoapModels(customer[] models) {
		customerSoap[] soapModels = new customerSoap[models.length];

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModel(models[i]);
		}

		return soapModels;
	}

	public static customerSoap[][] toSoapModels(customer[][] models) {
		customerSoap[][] soapModels = null;

		if (models.length > 0) {
			soapModels = new customerSoap[models.length][models[0].length];
		}
		else {
			soapModels = new customerSoap[0][0];
		}

		for (int i = 0; i < models.length; i++) {
			soapModels[i] = toSoapModels(models[i]);
		}

		return soapModels;
	}

	public static customerSoap[] toSoapModels(List<customer> models) {
		List<customerSoap> soapModels = new ArrayList<customerSoap>(models.size());

		for (customer model : models) {
			soapModels.add(toSoapModel(model));
		}

		return soapModels.toArray(new customerSoap[soapModels.size()]);
	}

	public customerSoap() {
	}

	public int getPrimaryKey() {
		return _Id;
	}

	public void setPrimaryKey(int pk) {
		setId(pk);
	}

	public int getId() {
		return _Id;
	}

	public void setId(int Id) {
		_Id = Id;
	}

	public String getFname() {
		return _fname;
	}

	public void setFname(String fname) {
		_fname = fname;
	}

	public String getLname() {
		return _lname;
	}

	public void setLname(String lname) {
		_lname = lname;
	}

	public String getMname() {
		return _mname;
	}

	public void setMname(String mname) {
		_mname = mname;
	}

	public int getAge() {
		return _age;
	}

	public void setAge(int age) {
		_age = age;
	}

	public Date getBirthdate() {
		return _birthdate;
	}

	public void setBirthdate(Date birthdate) {
		_birthdate = birthdate;
	}

	public int getPhone() {
		return _phone;
	}

	public void setPhone(int phone) {
		_phone = phone;
	}

	public String getCutomercode() {
		return _cutomercode;
	}

	public void setCutomercode(String cutomercode) {
		_cutomercode = cutomercode;
	}

	public String getEmai_id() {
		return _emai_id;
	}

	public void setEmai_id(String emai_id) {
		_emai_id = emai_id;
	}

	public String getGender() {
		return _gender;
	}

	public void setGender(String gender) {
		_gender = gender;
	}

	public String getAddress() {
		return _address;
	}

	public void setAddress(String address) {
		_address = address;
	}

	public String getLanguage() {
		return _language;
	}

	public void setLanguage(String language) {
		_language = language;
	}

	public String getCountry() {
		return _country;
	}

	public void setCountry(String country) {
		_country = country;
	}

	public String getState() {
		return _state;
	}

	public void setState(String state) {
		_state = state;
	}

	public String getCity() {
		return _city;
	}

	public void setCity(String city) {
		_city = city;
	}

	public String getMeal_choice() {
		return _meal_choice;
	}

	public void setMeal_choice(String meal_choice) {
		_meal_choice = meal_choice;
	}

	public String getMeal_preference() {
		return _meal_preference;
	}

	public void setMeal_preference(String meal_preference) {
		_meal_preference = meal_preference;
	}

	private int _Id;
	private String _fname;
	private String _lname;
	private String _mname;
	private int _age;
	private Date _birthdate;
	private int _phone;
	private String _cutomercode;
	private String _emai_id;
	private String _gender;
	private String _address;
	private String _language;
	private String _country;
	private String _state;
	private String _city;
	private String _meal_choice;
	private String _meal_preference;
}