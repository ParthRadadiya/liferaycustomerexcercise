package prelogin;

import org.osgi.framework.BundleActivator;
import org.osgi.framework.BundleContext;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.persistence.PortletUtil;

/**
 * @author parth.radadiya
 */
@Component(
		immediate=true,
		property={"key=login.events.pre"},
		service=LifecycleAction.class
		)

public class preloginActivator implements LifecycleAction {
	private final Log log=LogFactoryUtil.getLog(preloginActivator.class);

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {

		log.info("pre login event called");
		
		
		
	}
	

}