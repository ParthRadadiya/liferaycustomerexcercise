package LogoutActionhook.portlet;

import LogoutActionhook.constants.LogoutActionhookPortletKeys;

import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.struts.BaseStrutsAction;
import com.liferay.portal.kernel.struts.StrutsAction;

import javax.portlet.Portlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;

/**
 * @author parth.radadiya
 */
@Component(
	immediate = true,
	property = {
		"path=/portal/logout"
	},
	service = StrutsAction.class
)
public class LogoutActionhookPortlet extends BaseStrutsAction {
	
	@Override
	public String execute(HttpServletRequest request, HttpServletResponse response) throws Exception {
	
			System.out.println("Customize strut action hook");
			
		return super.execute(request, response);
	}
}