package CustomerCrud.portlet;

import CustomerCrud.constants.CustomerCrudPortletKeys;
import CustomerDetail.model.customer;
import CustomerDetail.service.customerLocalService;
import CustomerDetail.service.customerLocalServiceUtil;

import com.liferay.counter.kernel.service.CounterLocalServiceUtil;
import com.liferay.portal.kernel.dao.orm.DynamicQuery;
import com.liferay.portal.kernel.dao.orm.DynamicQueryFactory;
import com.liferay.portal.kernel.dao.orm.PropertyFactory;
import com.liferay.portal.kernel.dao.orm.RestrictionsFactory;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.portlet.PortletClassLoaderUtil;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.util.ParamUtil;

import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.Portlet;
import javax.portlet.PortletException;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author parth.radadiya
 */
@Component(
	immediate = true,
	property = {
		"com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true",
		"javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + CustomerCrudPortletKeys.CustomerCrud,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user"
	},
	service = Portlet.class
)
public class CustomerCrudPortlet extends MVCPortlet {
	@Reference
	customerLocalService customerservice;
	/*@Reference
	DynamicQueryFactory dynamicQueryFactory;
	@Reference
	PropertyFactory propertyFactory;
	@Reference
	RestrictionsFactory restrictionsFactory;*/

	private static final Log LOG = LogFactoryUtil.getLog(CustomerCrudPortlet.class);

	public void Add(ActionRequest req, ActionResponse res) throws Exception {

		LOG.info(ParamUtil.getString(req, "fname"));
		LOG.info(ParamUtil.getString(req, "lname"));
		LOG.info(ParamUtil.getString(req, "mname"));
		LOG.info(ParamUtil.getString(req, "gender"));
		LOG.info(ParamUtil.getString(req, "language"));
		LOG.info(ParamUtil.getString(req, "phone"));
		LOG.info(ParamUtil.getString(req, "email"));
		LOG.info(ParamUtil.getString(req, "birthdate"));
		LOG.info(ParamUtil.getString(req, "age"));
		LOG.info(ParamUtil.getString(req, "address"));
		LOG.info(ParamUtil.getString(req, "country"));
		LOG.info(ParamUtil.getString(req, "state"));
		LOG.info(ParamUtil.getString(req, "city"));
		LOG.info(ParamUtil.getString(req, "meal-choice"));
		LOG.info(ParamUtil.getString(req, "meal-preference"));
		LOG.info(ParamUtil.getString(req, "customercode"));

		customer cadd = customerLocalServiceUtil.createcustomer((int) CounterLocalServiceUtil.increment());

		cadd.setFname(ParamUtil.getString(req, "fname"));
		cadd.setLname(ParamUtil.getString(req, "lname"));
		cadd.setMname(ParamUtil.getString(req, "mname"));
		cadd.setGender(ParamUtil.getString(req, "gender"));
		cadd.setLanguage(String.join(",", ParamUtil.getStringValues(req, "language")));
		cadd.setPhone(ParamUtil.getInteger(req, "phone"));
		cadd.setEmai_id(ParamUtil.getString(req, "email"));
		Date dob = new SimpleDateFormat("yyyy-MM-dd").parse(ParamUtil.getString(req, "birthdate", ""));
		cadd.setBirthdate(dob);
		cadd.setAge(ParamUtil.getInteger(req, "age"));
		cadd.setAddress(ParamUtil.getString(req, "address"));
		cadd.setCountry(ParamUtil.getString(req, "country"));
		cadd.setState(ParamUtil.getString(req, "state"));
		cadd.setCity(ParamUtil.getString(req, "city"));
		cadd.setMeal_choice(ParamUtil.getString(req, "meal-choice"));
		cadd.setMeal_preference(ParamUtil.getString(req, "meal-preference"));
		cadd.setCutomercode(ParamUtil.getString(req, "customercode"));

		customerLocalServiceUtil.addcustomer(cadd);
	}

	/*
	 * ...................................UpdateMethod..............................
	 * .........................
	 */
	public void UpateData(ActionRequest req, ActionResponse res) throws Exception {
		String id = req.getParameter("Id");
		LOG.info("Id Found" + id);
		customer cupdate = customerLocalServiceUtil.fetchcustomer(Integer.parseInt(id));

		LOG.info("Id Found" + ParamUtil.getInteger(req, "Id"));
		cupdate.setFname(ParamUtil.getString(req, "fname"));
		cupdate.setLname(ParamUtil.getString(req, "lname"));
		cupdate.setMname(ParamUtil.getString(req, "mname"));
		cupdate.setGender(ParamUtil.getString(req, "gender"));
		cupdate.setLanguage(String.join(",", ParamUtil.getStringValues(req, "language")));
		cupdate.setPhone(ParamUtil.getInteger(req, "phone"));
		cupdate.setEmai_id(ParamUtil.getString(req, "email"));
		Date dob = new SimpleDateFormat("yyyy-MM-dd").parse(ParamUtil.getString(req, "birthdate", ""));
		cupdate.setBirthdate(dob);
		cupdate.setAge(ParamUtil.getInteger(req, "age"));
		cupdate.setAddress(ParamUtil.getString(req, "address"));
		cupdate.setCountry(ParamUtil.getString(req, "country"));
		cupdate.setState(ParamUtil.getString(req, "state"));
		cupdate.setCity(ParamUtil.getString(req, "city"));
		cupdate.setMeal_choice(ParamUtil.getString(req, "meal-choice"));
		cupdate.setMeal_preference(ParamUtil.getString(req, "meal-preference"));
		cupdate.setCutomercode(ParamUtil.getString(req, "customercode"));

		customerLocalServiceUtil.updatecustomer(cupdate);
	}

	/*
	 * ...................................findbymethod..............................
	 * .........................
	 */

	public void findByCustomer(ActionRequest req, ActionResponse res) throws Exception {

		LOG.info("findByCustomer Method called");
//
//		LOG.info("First name : " + ParamUtil.getString(req, "fname"));
//
//		
///*		 * List<customer> cfind = customerLocalServiceUtil.getCustomerfname("pfg");
//		 * 
//		 * LOG.info("First name : " + cfind);
//*/		 
//
		List<customer> cfindvegmeal = customerLocalServiceUtil
				.getCustomerMealChoice(ParamUtil.getString(req, "meal-choice"));

		req.setAttribute("customer", cfindvegmeal);
//
		LOG.info("Veg. : " + cfindvegmeal);
//
//		/* LOG.info("Non-Veg. : " + cfindnonmeal); 
//
	}
//
	/*
	 * ..................................................DeleteMethod...............
	 * ...........................
	 */

	public void delete(ActionRequest req, ActionResponse res) throws Exception {

		LOG.info("Delete Method called");

		customer cdelete = customerLocalServiceUtil.deletecustomer(ParamUtil.getInteger(req, "Id"));

		LOG.info("Deleted this Id" + cdelete);

	}

	/*
	 * ..................................................UpdateURLMethod............
	 * ... ...........................
	 */

	public void UpdateURL(ActionRequest req, ActionResponse res) throws Exception {
		String Id1 = ParamUtil.getString(req, "Id");
		try {
			customer cobj = customerLocalServiceUtil.getcustomer(Integer.parseInt(Id1));
			req.setAttribute("cobj", cobj);
			LOG.info("ID :" + cobj);
		} catch (NumberFormatException e) {
			e.printStackTrace();
		} catch (PortalException e) {

			e.printStackTrace();
		}

	}

	/*
	 * ..................................................DynamicQuery...............
	 * ...........................
	 */
	/*public void findByCustomer(ActionRequest renderRequest, ActionResponse renderResponse)
			throws IOException, PortletException {
		DynamicQuery dynamic = dynamicQueryFactory.forClass(customer.class,PortletClassLoaderUtil.getClassLoader());
		dynamic.add(propertyFactory.forName("fname").eq(new Integer("parth")));
		List<customer> dynamiccustomer = customerservice.dynamicQuery(dynamic);
		LOG.info("dynamic search list : " + dynamiccustomer);
		
	}*/
	
}