package AuthFilter;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.security.auth.AuthException;
import com.liferay.portal.kernel.security.auth.Authenticator;
import com.liferay.portal.kernel.service.UserLocalService;

import java.util.Map;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "key=auth.pipeline.pre" }, service = Authenticator.class)
public class AuthFilter implements Authenticator {

	@Reference
	UserLocalService user;

	private static final Log LOG = LogFactoryUtil.getLog(AuthFilter.class);

	@Override
	public int authenticateByEmailAddress(long companyId, String emailAddress, String password,
			Map<String, String[]> headerMap, Map<String, String[]> parameterMap) throws AuthException {
		LOG.info("authenticateByEmailAddress Method called");
		return FAILURE;
	}

	@Override
	public int authenticateByScreenName(long companyId, String screenName, String password,
			Map<String, String[]> headerMap, Map<String, String[]> parameterMap) throws AuthException {
		LOG.info("authenticateByScreenName Method called");
		return FAILURE;
	}

	@Override
	public int authenticateByUserId(long companyId, long userId, String password, Map<String, String[]> headerMap,
			Map<String, String[]> parameterMap) throws AuthException {

		LOG.info("authenticateByUserId Method called");

		String userid = String.valueOf(userId);

		if (userid.startsWith(userid, 20)) {

			return SUCCESS;
		} else {

			LOG.info("authenticateByUserId Method is not called");
		}

		return SUCCESS;

	}

}