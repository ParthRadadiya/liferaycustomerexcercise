package ResourceFilterSample.portlet;

import TestCustomerForm.constants.TestCustomerFormPortletKeys;

import java.io.IOException;

import javax.portlet.PortletException;
import javax.portlet.ResourceRequest;
import javax.portlet.ResourceResponse;
import javax.portlet.filter.*;
import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;


/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = {
		"javax.portlet.name=" + TestCustomerFormPortletKeys.TestCustomerForm,	
		}, service = PortletFilter.class)

public class ResourceFilterSamplePortlet implements ResourceFilter {

	private static final Log LOG = LogFactoryUtil.getLog(ResourceFilterSamplePortlet.class);

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
		LOG.info("Init Method called");
	}

	@Override
	public void destroy() {
		LOG.info("Detroy Method called");
	}

	@Override
	public void doFilter(ResourceRequest request, ResourceResponse response, FilterChain chain)
			throws IOException, PortletException {
		LOG.info("DoFilter Method called");
		chain.doFilter(request, response);
	}
	
}