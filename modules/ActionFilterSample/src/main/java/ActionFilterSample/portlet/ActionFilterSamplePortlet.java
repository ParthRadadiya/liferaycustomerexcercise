package ActionFilterSample.portlet;

import TestCustomerForm.constants.TestCustomerFormPortletKeys;

import java.io.IOException;

import javax.portlet.ActionRequest;
import javax.portlet.ActionResponse;
import javax.portlet.PortletException;
import javax.portlet.filter.ActionFilter;
import javax.portlet.filter.FilterChain;
import javax.portlet.filter.FilterConfig;
import javax.portlet.filter.PortletFilter;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = {
		"javax.portlet.name=" + TestCustomerFormPortletKeys.TestCustomerForm, }, service = PortletFilter.class)
public class ActionFilterSamplePortlet implements ActionFilter {

	private static final Log LOG = LogFactoryUtil.getLog(ActionFilterSamplePortlet.class);

	@Override
	public void init(FilterConfig filterConfig) throws PortletException {
		LOG.info("InitAction Method called");
	}

	@Override
	public void destroy() {
		LOG.info("DestoryAction Method called");
	}

	@Override
	public void doFilter(ActionRequest request, ActionResponse response, FilterChain chain)
			throws IOException, PortletException {
		LOG.info("doFilterAction Method called");
		chain.doFilter(request, response);
	}

}