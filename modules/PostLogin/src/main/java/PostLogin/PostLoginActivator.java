package PostLogin;

import org.osgi.service.component.annotations.Component;

import com.liferay.portal.kernel.events.ActionException;
import com.liferay.portal.kernel.events.LifecycleAction;
import com.liferay.portal.kernel.events.LifecycleEvent;
import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;


/**
 * @author parth.radadiya
 */
@Component(
	
	immediate=true,
	property= {"key=login.events.post"},
	service=LifecycleAction.class
)

public class PostLoginActivator implements LifecycleAction {
	
	private final Log log=LogFactoryUtil.getLog(PostLoginActivator.class);

	@Override
	public void processLifecycleEvent(LifecycleEvent lifecycleEvent) throws ActionException {
		
		log.info("Post login event called");
		
		User user=(User) lifecycleEvent.getRequest();
		
		log.info(user.getScreenName());
		
	}
	

}