package userWrapperService;

import com.liferay.portal.kernel.service.UserLocalServiceWrapper;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.service.ServiceWrapper;

import org.osgi.service.component.annotations.Component;

/**
 * @author parth.radadiya
 */
@Component(
	immediate = true,
	property = {
	},
	service = ServiceWrapper.class
)
public class UserWrapperService extends UserLocalServiceWrapper {

	public UserWrapperService() {
		
			
		super(null);
	}
	
	@Override
		public User getUser(long userId) throws PortalException {
	
			return super.getUser(userId);
		}

}