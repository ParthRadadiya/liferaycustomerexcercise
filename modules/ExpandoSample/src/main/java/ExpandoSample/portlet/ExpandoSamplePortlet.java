package ExpandoSample.portlet;

import ExpandoSample.constants.ExpandoSamplePortletKeys;

import com.liferay.expando.kernel.model.ExpandoColumn;
import com.liferay.expando.kernel.model.ExpandoColumnConstants;
import com.liferay.expando.kernel.model.ExpandoRow;
import com.liferay.expando.kernel.model.ExpandoTable;
import com.liferay.expando.kernel.service.ExpandoColumnLocalService;
import com.liferay.expando.kernel.service.ExpandoRowLocalService;
import com.liferay.expando.kernel.service.ExpandoTableLocalService;
import com.liferay.portal.kernel.exception.PortalException;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.model.User;
import com.liferay.portal.kernel.portlet.bridges.mvc.MVCPortlet;
import com.liferay.portal.kernel.service.ClassNameLocalService;
import com.liferay.portal.kernel.theme.ThemeDisplay;
import com.liferay.portal.kernel.util.WebKeys;

import java.io.IOException;

import javax.portlet.Portlet;
import javax.portlet.PortletException;
import javax.portlet.RenderRequest;
import javax.portlet.RenderResponse;

import org.osgi.service.component.annotations.Component;
import org.osgi.service.component.annotations.Reference;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "com.liferay.portlet.display-category=category.sample",
		"com.liferay.portlet.instanceable=true", "javax.portlet.init-param.template-path=/",
		"javax.portlet.init-param.view-template=/view.jsp",
		"javax.portlet.name=" + ExpandoSamplePortletKeys.ExpandoSample,
		"javax.portlet.resource-bundle=content.Language",
		"javax.portlet.security-role-ref=power-user,user" }, service = Portlet.class)
public class ExpandoSamplePortlet extends MVCPortlet {

	private static final com.liferay.portal.kernel.log.Log log = LogFactoryUtil.getLog(ExpandoSamplePortlet.class);

	@Reference
	ExpandoTableLocalService expandoTableLocalService;
	@Reference
	ExpandoColumnLocalService expandoColumnLocalService;
	@Reference
	ExpandoRowLocalService expandoRowLocalService;
	@Reference
	ClassNameLocalService classnameLocalService;

	@Override
	public void render(RenderRequest renderRequest, RenderResponse renderResponse)
			throws IOException, PortletException {
		log.info("Render Method called");

		long classNameId = classnameLocalService.getClassNameId(User.class.getName());
		ThemeDisplay themeDisplay = (ThemeDisplay) renderRequest.getAttribute(WebKeys.THEME_DISPLAY);

		try {
			ExpandoTable expandoTable = expandoTableLocalService.getTable(themeDisplay.getCompanyId(), classNameId,
					"MyUser");
			log.info("Get Expando Table" + expandoTable);
			ExpandoColumn expandoColumn = expandoColumnLocalService.addColumn(expandoTable.getTableId(), "new column",
					ExpandoColumnConstants.STRING);
			log.info("Get Expando Column" + expandoColumn);
			ExpandoRow expandoRow = expandoRowLocalService.addRow(expandoTable.getTableId(), themeDisplay.getUserId());
			log.info("Get Expando Row" + expandoRow);
		} catch (PortalException e) {
			e.printStackTrace();
		}
		super.render(renderRequest, renderResponse);
	}
}