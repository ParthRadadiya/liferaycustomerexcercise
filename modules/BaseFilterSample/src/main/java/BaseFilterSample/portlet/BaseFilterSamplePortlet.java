package BaseFilterSample.portlet;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.osgi.service.component.annotations.Component;


import com.liferay.portal.kernel.log.Log;
import com.liferay.portal.kernel.log.LogFactoryUtil;
import com.liferay.portal.kernel.servlet.BaseFilter;

/**
 * @author parth.radadiya
 */
@Component(immediate = true, property = { "servlet-context-name=", 
		"servlet-filter-name=http-filter",
		"url-pattern=/web/guest/home"  }, service = Filter.class)
public class BaseFilterSamplePortlet extends BaseFilter {

	private static final Log LOG = LogFactoryUtil.getLog(BaseFilterSamplePortlet.class);

	@Override
	protected Log getLog() {
		return LOG;
	}

	@Override
	public void init(FilterConfig filterConfig) {
		LOG.info("Init method called");
		super.init(filterConfig);
	}

	@Override
	public void destroy() {
		LOG.info("Destroy method called");
		super.destroy();
	}

	@Override
	public void doFilter(ServletRequest servletRequest, ServletResponse servletResponse, FilterChain filterChain)
			throws IOException, ServletException {
		LOG.info("DoFilter method called");
		super.doFilter(servletRequest, servletResponse, filterChain);
	}

	@Override
	public FilterConfig getFilterConfig() {
		LOG.info("FilterConfig method called");
		return super.getFilterConfig();
	}

	@Override
	public boolean isFilterEnabled() {
		LOG.info("isFilterEnabled method called");
		return super.isFilterEnabled();
	}

	@Override
	protected void processFilter(HttpServletRequest request, HttpServletResponse response, FilterChain filterChain)
			throws Exception {
		LOG.info("processFilter method called");
		super.processFilter(request, response, filterChain);
	}

}